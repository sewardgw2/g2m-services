package com.g2m.services.strategybuilder.testdata;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.g2m.services.tradingservices.enums.BarSize;

/**
 * Added 3/1/2015.
 * 
 * @author Michael Borromeo
 */
public class BarTestData {
	/**
	 * The test bars created here will have their open/close/high/low values set to the sequence
	 * number in which they come. E.g. The second bar generated will have its open/close/high/low
	 * values set to 2.
	 */
	public static List<com.ib.controller.Bar> createTestBars(Date startDateTime, BarSize barSize, int count) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(startDateTime);
		List<com.ib.controller.Bar> bars = new ArrayList<com.ib.controller.Bar>();
		for (int i = 1; i <= count; i++) {
			com.ib.controller.Bar bar = new com.ib.controller.Bar(calendar.getTimeInMillis(), (double) i, (double) i,
					(double) i, (double) i, (double) i, (long) i, i);
			bars.add(bar);
			calendar.add(Calendar.SECOND, barSize.getSecondsInBar());
		}
		return bars;
	}
}
