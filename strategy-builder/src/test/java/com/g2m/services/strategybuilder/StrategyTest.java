package com.g2m.services.strategybuilder;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.reflect.internal.WhiteboxImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.g2m.services.strategybuilder.Strategy.TickAndBarSubscriber;
import com.g2m.services.strategybuilder.enums.EntityPersistMode;
import com.g2m.services.strategybuilder.enums.StrategyStatus;
import com.g2m.services.strategybuilder.utilities.TestContextConfiguration;
import com.g2m.services.tradingservices.BarPublisher;
import com.g2m.services.tradingservices.BarPublisher.BarPublisherData;
import com.g2m.services.tradingservices.SecurityRegistry;
import com.g2m.services.tradingservices.TickDispatcher;
import com.g2m.services.tradingservices.TickSubscriber;
import com.g2m.services.tradingservices.backtest.BacktestAccount;
import com.g2m.services.tradingservices.backtest.BacktestTickPublisher;
import com.g2m.services.tradingservices.backtest.BacktestTrader;
import com.g2m.services.tradingservices.backtest.BacktestTradingService;
import com.g2m.services.tradingservices.brokerage.BrokerageAccount;
import com.g2m.services.tradingservices.brokerage.BrokerageConnection;
import com.g2m.services.tradingservices.brokerage.BrokerageTrader;
import com.g2m.services.tradingservices.brokerage.BrokerageTrader.AllOrdersHandler;
import com.g2m.services.tradingservices.brokerage.BrokerageTradingService;
import com.g2m.services.tradingservices.caches.TickCache;
import com.g2m.services.tradingservices.entities.Bar;
import com.g2m.services.tradingservices.entities.Bar.BarBuilder;
import com.g2m.services.tradingservices.entities.Security;
import com.g2m.services.tradingservices.entities.Security.SecurityBuilder;
import com.g2m.services.tradingservices.entities.Security.SecurityKey;
import com.g2m.services.tradingservices.entities.Tick;
import com.g2m.services.tradingservices.entities.Tick.TickBuilder;
import com.g2m.services.tradingservices.entities.orders.LimitOrder;
import com.g2m.services.tradingservices.entities.orders.MarketOrder;
import com.g2m.services.tradingservices.entities.orders.Order;
import com.g2m.services.tradingservices.enums.BarSize;
import com.g2m.services.tradingservices.enums.OrderAction;
import com.g2m.services.tradingservices.enums.OrderStatus;
import com.g2m.services.tradingservices.enums.OrderStatusReason;
import com.g2m.services.tradingservices.enums.SecurityType;
import com.g2m.services.tradingservices.persistence.BarPersistThread;
import com.g2m.services.tradingservices.persistence.OrderPersistThread;
import com.g2m.services.tradingservices.persistence.TickPersistThread;
import com.g2m.services.variables.VariableService;
import com.g2m.services.variables.entities.MovingAverage;
import com.g2m.services.variables.entities.MovingAverage.MovingAverageBuilder;
import com.g2m.services.variables.entities.Variable;
import com.g2m.services.variables.persistence.VariablePersistThread;
import com.ib.controller.ApiController;

/**
 * Added 5/27/2015.
 * 
 * @author michaelborromeo
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TestContextConfiguration.class)
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class StrategyTest {
	@Autowired
	private BacktestTradingService backtestTradingService;
	@Autowired
	private BrokerageTradingService brokerageTradingService;
	@Autowired
	private BacktestTrader backtestTrader;
	@Autowired
	private BrokerageTrader brokerageTrader;
	@Autowired
	private TestStrategyImpl strategy;
	@Autowired
	private TickDispatcher tickDispatcher;
	@Autowired
	private BarPublisher barPublisher;
	@Autowired
	private BacktestTickPublisher backtestTickPublisher;
	private BacktestAccount backtestAccount;
	private BrokerageAccount brokerageAccount;
	private ApiController apiController;
	private BrokerageConnection brokerageConnection;
	private TickCache tickCache;
	private VariableService variableService;
	private TickPersistThread tickPersistThread;
	private BarPersistThread barPersistThread;
	private VariablePersistThread variablePersistThread;
	private OrderPersistThread orderPersistThread;
	private Order marketOrder;
	private Order limitOrder;
	private Security security;
	private Tick tick;
	private Bar bar;
	private MovingAverage movingAverage;
	private double price;

	@Before
	public void setup() {
		price = 50;
		setupSecurities();
		setupOrders();
		setupTicks();
		setupBars();
		setupVariables();
		setupMocks();
	}

	private void setupSecurities() {
		SecurityRegistry.clear();
		SecurityBuilder securityBuilder = new SecurityBuilder();
		securityBuilder.setSymbol("ES");
		securityBuilder.setExchange("GLOBEX");
		securityBuilder.setExpiry(2015, Calendar.JUNE);
		securityBuilder.setSecurityType(SecurityType.FUTURE);
		security = securityBuilder.build();
	}

	private void setupOrders() {
		marketOrder = new MarketOrder(OrderAction.BUY, 1, security);
		limitOrder = new LimitOrder(OrderAction.BUY, 1, security, price - 1);
	}

	private void setupTicks() {
		TickBuilder tickBuilder = new TickBuilder();
		tickBuilder.setAskPrice(0);
		tickBuilder.setBidPrice(0);
		tickBuilder.setChange(0);
		tickBuilder.setDateTime(new Date());
		tickBuilder.setLastPrice(0);
		tickBuilder.setOpenInterest(0);
		tickBuilder.setSecurity(security);
		tickBuilder.setVolume(0);
		tickBuilder.setVolume(0);
		tickBuilder.setVolumeBid(0);
		tick = tickBuilder.build();
	}

	private void setupBars() {
		BarBuilder barBuilder = new BarBuilder();
		barBuilder.setSecurity(security);
		barBuilder.setBarSize(BarSize._10_MINS);
		barBuilder.setDateTime(new Date());
		barBuilder.setHigh(0);
		barBuilder.setLow(0);
		barBuilder.setOpen(0);
		barBuilder.setClose(0);
		barBuilder.setVolume(0);
		bar = barBuilder.build();
	}

	private void setupVariables() {
		MovingAverageBuilder builder = new MovingAverageBuilder();
		builder.setBarSize(BarSize._10_MINS);
		builder.setCalculated(true);
		builder.setCalculatedOnDateTime(new Date());
		builder.setDateTime(new Date());
		builder.setMovingAverage(0);
		builder.setPeriod(1);
		builder.setSecurityKey(security.getKey());
		movingAverage = builder.build();
	}

	/**
	 * Doing this here rather than using @InjectMocks and initMocks() due to complexity of
	 * dependency setup; it's just easier this way.
	 */
	private void setupMocks() {
		// BacktestAccount
		backtestAccount = Mockito.mock(BacktestAccount.class);
		Mockito.when(backtestAccount.canFillOrder(Matchers.any(Order.class))).thenReturn(OrderStatusReason.OK_TO_FILL);
		WhiteboxImpl.setInternalState(backtestTradingService, "account", backtestAccount);
		WhiteboxImpl.setInternalState(backtestTrader, "account", backtestAccount);

		// BrokerageAccount
		brokerageAccount = Mockito.mock(BrokerageAccount.class);
		WhiteboxImpl.setInternalState(brokerageTradingService, "account", brokerageAccount);

		// BrokerageConnection
		apiController = Mockito.mock(ApiController.class);
		brokerageConnection = Mockito.mock(BrokerageConnection.class);
		Mockito.when(brokerageConnection.getAccountCodes()).thenReturn(Arrays.asList("12345"));
		Mockito.when(brokerageConnection.getApiController()).thenReturn(apiController);
		WhiteboxImpl.setInternalState(brokerageTradingService, "connection", brokerageConnection);

		// BrokerageTrader
		WhiteboxImpl.setInternalState(brokerageTrader, "connection", brokerageConnection);

		// TickCache
		tickCache = Mockito.mock(TickCache.class);
		Mockito.when(tickCache.getLastTickPrice(Matchers.any(SecurityKey.class))).thenReturn(price);
		WhiteboxImpl.setInternalState(backtestTrader, "tickCache", tickCache);
		WhiteboxImpl.setInternalState(backtestAccount, "tickCache", tickCache);

		// VariableService
		variableService = Mockito.mock(VariableService.class);
		Mockito.when(variableService.get(Matchers.any(), Matchers.any())).thenReturn(movingAverage);
		WhiteboxImpl.setInternalState(strategy, "variableService", variableService);

		// PersistThreads
		tickPersistThread = Mockito.mock(TickPersistThread.class);
		WhiteboxImpl.setInternalState(strategy, "tickPersistThread", tickPersistThread);
		barPersistThread = Mockito.mock(BarPersistThread.class);
		WhiteboxImpl.setInternalState(strategy, "barPersistThread", barPersistThread);
		variablePersistThread = Mockito.mock(VariablePersistThread.class);
		WhiteboxImpl.setInternalState(strategy, "variablePersistThread", variablePersistThread);
		orderPersistThread = Mockito.spy(OrderPersistThread.class);
		WhiteboxImpl.setInternalState(strategy, "orderPersistThread", orderPersistThread);
		Mockito.doNothing().when(orderPersistThread).persist(Matchers.any(Order.class));
	}

	@Test
	public void startTestTwice_Backtest() {
		assertEquals(StrategyStatus.NOT_STARTED, strategy.getStrategyStatus());
		strategy.startBacktest();
		// since backtesting is blocking, it will complete before returning
		assertEquals(StrategyStatus.STOPPED, strategy.getStrategyStatus());
		try {
			strategy.startBacktest();
			Assert.fail("Expected an exception from starting a strategy more than once.");
		} catch (Exception e) {
		}
	}

	@Test
	public void startTestTwice_Live() {
		assertEquals(StrategyStatus.NOT_STARTED, strategy.getStrategyStatus());
		strategy.start("host", 1, 1);
		assertEquals(StrategyStatus.STARTED_LIVE, strategy.getStrategyStatus());
		try {
			strategy.start("host", 1, 1);
			Assert.fail("Expected an exception from starting a strategy more than once.");
		} catch (Exception e) {
		}
		strategy.stop();
		assertEquals(StrategyStatus.STOPPED, strategy.getStrategyStatus());
	}

	@Test
	public void submitOrderTest_Backtest() {
		try {
			strategy.submitOrder(marketOrder);
			Assert.fail("Expected an exception from submitting an order before starting the strategy.");
		} catch (Exception e) {
		}
		strategy.startBacktest();
		/*
		 * Since backtests are blocking, we need to fake a started strategy to avoid needing to
		 * setup test files as well as to avoid having to put this logic in an actual strategy
		 */
		WhiteboxImpl.setInternalState(strategy, "status", StrategyStatus.STARTED_BACKTEST);
		strategy.subscribeToTicks(security);
		strategy.submitOrder(marketOrder);
		List<Order> orders = backtestTrader.getOrders(security.getKey());
		assertEquals(1, orders.size());
		assertEquals(marketOrder.getOrderAction(), orders.get(0).getOrderAction());
		assertEquals(marketOrder.getOrderType(), orders.get(0).getOrderType());
		assertEquals(marketOrder.getSecurityKey(), orders.get(0).getSecurityKey());
		assertEquals(marketOrder.getQuantity(), orders.get(0).getQuantity());
		assertEquals(OrderStatus.FILLED, orders.get(0).getLatestOrderState().getOrderStatus());
	}

	@Test
	public void submitOrderTest_Live() {
		try {
			strategy.submitOrder(marketOrder);
			Assert.fail("Expected an exception from submitting an order before starting the strategy.");
		} catch (Exception e) {
		}
		strategy.subscribeToTicks(security);
		strategy.start("host", 123, 1);
		strategy.submitOrder(marketOrder);
		List<Order> orders = brokerageTrader.getOrders(security.getKey());
		assertEquals(1, orders.size());
		assertEquals(marketOrder.getOrderAction(), orders.get(0).getOrderAction());
		assertEquals(marketOrder.getOrderType(), orders.get(0).getOrderType());
		assertEquals(marketOrder.getSecurityKey(), orders.get(0).getSecurityKey());
		assertEquals(marketOrder.getQuantity(), orders.get(0).getQuantity());
		assertEquals(OrderStatus.PRE_SUBMITTED, orders.get(0).getLatestOrderState().getOrderStatus());
		strategy.stop();
	}

	@Test
	public void submitOrderWithoutRegisteringSecurityTest_Backtest() {
		strategy.startBacktest();
		try {
			strategy.submitOrder(marketOrder);
			Assert.fail("Expected an exception from submitting an order for a security that's not yet registered.");
		} catch (Exception e) {
		}
	}

	@Test
	public void submitOrderWithoutRegisteringSecurityTest_Live() {
		strategy.start("host", 123, 1);
		try {
			strategy.submitOrder(marketOrder);
			Assert.fail("Expected an exception from submitting an order for a security that's not yet registered.");
		} catch (Exception e) {
		}
		strategy.stop();
	}

	@Test
	public void cancelOrderTest_Backtest() {
		try {
			strategy.cancelOrder(limitOrder.getKey());
			Assert.fail("Expected an exception from cancelling an order before starting the strategy.");
		} catch (Exception e) {
		}
		SecurityRegistry.add(security);
		strategy.startBacktest();
		/*
		 * Since backtests are blocking, we need to fake a started strategy to avoid needing to
		 * setup test files as well as to avoid having to put this logic in an actual strategy
		 */
		WhiteboxImpl.setInternalState(strategy, "status", StrategyStatus.STARTED_BACKTEST);
		strategy.submitOrder(limitOrder);
		assertEquals(1, strategy.getOrders(security.getKey()).size());
		assertEquals(OrderStatus.SUBMITTED, strategy.getOrders(security.getKey()).get(0).getLatestOrderState()
				.getOrderStatus());
		strategy.cancelOrder(limitOrder.getKey());
		assertEquals(OrderStatus.CANCELLED, strategy.getOrders(security.getKey()).get(0).getLatestOrderState()
				.getOrderStatus());
	}

	@Test
	public void cancelOrderTest_Live() {
		try {
			strategy.cancelOrder(limitOrder.getKey());
			Assert.fail("Expected an exception from cancelling an order before starting the strategy.");
		} catch (Exception e) {
		}
		strategy.subscribeToTicks(security);
		strategy.start("host", 123, 1);
		strategy.submitOrder(limitOrder);
		assertEquals(1, strategy.getOrders(security.getKey()).size());
		assertEquals(OrderStatus.PRE_SUBMITTED, strategy.getOrders(security.getKey()).get(0).getLatestOrderState()
				.getOrderStatus());
		AllOrdersHandler allOrdersHandler = WhiteboxImpl.getInternalState(brokerageTrader, "allOrdersHandler");
		allOrdersHandler.orderStatus(0, com.ib.controller.OrderStatus.Cancelled, 0, 0, 0d, 0, 0, 0d, 0, "");
		Mockito.doAnswer(new Answer<Void>() {
			@Override
			public Void answer(InvocationOnMock invocation) throws Throwable {
				WhiteboxImpl.setInternalState(strategy.getOrders(security.getKey()).get(0).getLatestOrderState(),
					"orderStatus", OrderStatus.CANCELLED);
				return null;
			}
		}).when(apiController).cancelOrder(Matchers.anyInt());
		strategy.cancelOrder(limitOrder.getKey());
		assertEquals(OrderStatus.CANCELLED, strategy.getOrders(security.getKey()).get(0).getLatestOrderState()
				.getOrderStatus());
		strategy.stop();
	}

	@Test
	public void persistAllTest_Backtest() {
		strategy.setEntityPersistMode(EntityPersistMode.ALL);
		strategy.startBacktest();
		/*
		 * Since backtests are blocking, we need to fake a started strategy to avoid needing to
		 * setup test files as well as to avoid having to put this logic in an actual strategy
		 */
		WhiteboxImpl.setInternalState(strategy, "status", StrategyStatus.STARTED_BACKTEST);
		TickAndBarSubscriber tickAndBarSubscriber = WhiteboxImpl.getInternalState(strategy, "tickAndBarSubscriber");
		tickAndBarSubscriber.tickReceived(tick);
		tickAndBarSubscriber.barCreated(bar);
		strategy.getVariables(new MovingAverage(), new Date());
		SecurityRegistry.add(security);
		strategy.submitOrder(marketOrder);
		Mockito.verify(tickPersistThread, Mockito.times(1)).persist(Matchers.any(Tick.class));
		Mockito.verify(barPersistThread, Mockito.times(1)).persist(Matchers.any(Bar.class));
		Mockito.verify(variablePersistThread, Mockito.times(1)).persist(Matchers.isA(Variable.class));
		Mockito.verify(orderPersistThread, Mockito.times(1)).persist(Matchers.isA(Order.class));
	}

	@Test
	public void persistNoneTest_Backtest() {
		strategy.setEntityPersistMode(EntityPersistMode.NONE);
		strategy.startBacktest();
		/*
		 * Since backtests are blocking, we need to fake a started strategy to avoid needing to
		 * setup test files as well as to avoid having to put this logic in an actual strategy
		 */
		WhiteboxImpl.setInternalState(strategy, "status", StrategyStatus.STARTED_BACKTEST);
		TickAndBarSubscriber tickAndBarSubscriber = WhiteboxImpl.getInternalState(strategy, "tickAndBarSubscriber");
		tickAndBarSubscriber.tickReceived(tick);
		tickAndBarSubscriber.barCreated(bar);
		strategy.getVariables(new MovingAverage(), new Date());
		SecurityRegistry.add(security);
		strategy.submitOrder(marketOrder);
		Mockito.verify(tickPersistThread, Mockito.times(0)).persist(Matchers.any(Tick.class));
		Mockito.verify(barPersistThread, Mockito.times(0)).persist(Matchers.any(Bar.class));
		Mockito.verify(variablePersistThread, Mockito.times(0)).persist(Matchers.isA(Variable.class));
		Mockito.verify(orderPersistThread, Mockito.times(0)).persist(Matchers.isA(Order.class));
	}

	@Test
	public void persistAllTest_Live() {
		strategy.setEntityPersistMode(EntityPersistMode.ALL);
		strategy.start("host", 123, 1);
		TickAndBarSubscriber tickAndBarSubscriber = WhiteboxImpl.getInternalState(strategy, "tickAndBarSubscriber");
		tickAndBarSubscriber.tickReceived(tick);
		tickAndBarSubscriber.barCreated(bar);
		strategy.getVariables(new MovingAverage(), new Date());
		SecurityRegistry.add(security);
		strategy.submitOrder(marketOrder);
		Mockito.verify(tickPersistThread, Mockito.times(1)).persist(Matchers.any(Tick.class));
		Mockito.verify(barPersistThread, Mockito.times(1)).persist(Matchers.any(Bar.class));
		Mockito.verify(variablePersistThread, Mockito.times(1)).persist(Matchers.isA(Variable.class));
		Mockito.verify(orderPersistThread, Mockito.times(1)).persist(Matchers.isA(Order.class));
		strategy.stop();
	}

	@Test
	public void persistNoneTest_Live() {
		strategy.setEntityPersistMode(EntityPersistMode.NONE);
		strategy.start("host", 123, 1);
		TickAndBarSubscriber tickAndBarSubscriber = WhiteboxImpl.getInternalState(strategy, "tickAndBarSubscriber");
		tickAndBarSubscriber.tickReceived(tick);
		tickAndBarSubscriber.barCreated(bar);
		strategy.getVariables(new MovingAverage(), new Date());
		SecurityRegistry.add(security);
		strategy.submitOrder(marketOrder);
		Mockito.verify(tickPersistThread, Mockito.times(0)).persist(Matchers.any(Tick.class));
		Mockito.verify(barPersistThread, Mockito.times(0)).persist(Matchers.any(Bar.class));
		Mockito.verify(variablePersistThread, Mockito.times(0)).persist(Matchers.isA(Variable.class));
		Mockito.verify(orderPersistThread, Mockito.times(0)).persist(Matchers.isA(Order.class));
		strategy.stop();
	}

	@Test
	public void subscriberSetupTest_Backtest() {
		strategy.subscribeToTicks(security);
		strategy.subscribeToBars(security, BarSize._10_MINS);
		strategy.addTestFile(security, "asdf");
		strategy.startBacktest();
		Map<SecurityKey, List<TickSubscriber>> tickSubscriberMap = WhiteboxImpl.getInternalState(tickDispatcher,
			"tickSubscriberMap");
		TickAndBarSubscriber tickAndBarSubscriber = WhiteboxImpl.getInternalState(strategy, "tickAndBarSubscriber");
		assertTrue(tickSubscriberMap.containsKey(security.getKey()));
		assertTrue(tickSubscriberMap.get(security.getKey()).contains(tickAndBarSubscriber));

		Map<SecurityKey, Map<BarSize, BarPublisherData>> barPublishersData = WhiteboxImpl.getInternalState(barPublisher,
			"barPublishersData");
		assertTrue(barPublishersData.containsKey(security.getKey()));
		assertTrue(barPublishersData.get(security.getKey()).containsKey(BarSize._10_MINS));
		assertTrue(barPublishersData.get(security.getKey()).get(BarSize._10_MINS).getBarSubscribers()
				.contains(tickAndBarSubscriber));
	}

	@Test
	public void subscriberSetupTest_Live() {
		strategy.subscribeToTicks(security);
		strategy.subscribeToBars(security, BarSize._10_MINS);
		strategy.start("host", 123, 1);
		Map<SecurityKey, List<TickSubscriber>> tickSubscriberMap = WhiteboxImpl.getInternalState(tickDispatcher,
			"tickSubscriberMap");
		TickAndBarSubscriber tickAndBarSubscriber = WhiteboxImpl.getInternalState(strategy, "tickAndBarSubscriber");
		assertTrue(tickSubscriberMap.containsKey(security.getKey()));
		assertTrue(tickSubscriberMap.get(security.getKey()).contains(tickAndBarSubscriber));

		Map<SecurityKey, Map<BarSize, BarPublisherData>> barPublishersData = WhiteboxImpl.getInternalState(barPublisher,
			"barPublishersData");
		assertTrue(barPublishersData.containsKey(security.getKey()));
		assertTrue(barPublishersData.get(security.getKey()).containsKey(BarSize._10_MINS));
		assertTrue(barPublishersData.get(security.getKey()).get(BarSize._10_MINS).getBarSubscribers()
				.contains(tickAndBarSubscriber));
		strategy.stop();
	}

	@Test
	public void backtestFilesTest() {
		strategy.subscribeToTicks(security);
		strategy.subscribeToBars(security, BarSize._10_MINS);
		try {
			strategy.startBacktest();
			Assert.fail("Expected an exception from not having setup test data files.");
		} catch (Exception e) {
		}
		strategy.addTestFile(security, "asdf");
		strategy.startBacktest();

		Map<SecurityKey, String> testFilePaths = WhiteboxImpl.getInternalState(backtestTickPublisher, "testFilePaths");
		assertTrue(testFilePaths.containsKey(security.getKey()));
		assertTrue(testFilePaths.get(security.getKey()).equals("asdf"));
	}
}
