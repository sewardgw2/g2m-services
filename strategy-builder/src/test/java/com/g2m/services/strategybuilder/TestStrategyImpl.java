package com.g2m.services.strategybuilder;

import org.springframework.stereotype.Component;

import com.g2m.services.tradingservices.entities.Bar;
import com.g2m.services.tradingservices.entities.Tick;

/**
 * Added 5/29/2015.
 * 
 * @author michaelborromeo
 */
@Component
public class TestStrategyImpl extends Strategy {
	public TestStrategyImpl() {
		// no call to super()
	}

	@Override
	protected void run() {
	}

	@Override
	protected void tickReceived(Tick tick) {
	}

	@Override
	protected void barCreated(Bar bar) {
	}
}
