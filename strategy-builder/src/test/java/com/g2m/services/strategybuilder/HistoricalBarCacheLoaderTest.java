package com.g2m.services.strategybuilder;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.reflect.internal.WhiteboxImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.g2m.services.historicaldataloader.DataLoaderEventHandler;
import com.g2m.services.historicaldataloader.HistoricalDataLoader;
import com.g2m.services.strategybuilder.testdata.BarTestData;
import com.g2m.services.strategybuilder.utilities.TestContextConfiguration;
import com.g2m.services.tradingservices.caches.BarCache;
import com.g2m.services.tradingservices.entities.Security;
import com.g2m.services.tradingservices.entities.Security.SecurityBuilder;
import com.g2m.services.tradingservices.enums.BarSize;
import com.g2m.services.tradingservices.enums.SecurityType;

import static org.junit.Assert.assertEquals;

/**
 * Added 6/30/2015.
 * 
 * @author michaelborromeo
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TestContextConfiguration.class)
public class HistoricalBarCacheLoaderTest {
	@Autowired
	private HistoricalBarCacheLoader cacheLoader;
	@Autowired
	private BarCache barCache;
	private HistoricalDataLoader dataLoader;
	private Security security;

	@Before
	public void setup() {
		barCache.clear();
		SecurityBuilder securityBuilder = new SecurityBuilder();
		securityBuilder.setSymbol("ES");
		securityBuilder.setExchange("GLOBEX");
		securityBuilder.setExpiry(2015, Calendar.JUNE);
		securityBuilder.setSecurityType(SecurityType.FUTURE);
		security = securityBuilder.build();
		dataLoader = Mockito.mock(HistoricalDataLoader.class);
		WhiteboxImpl.setInternalState(cacheLoader, HistoricalDataLoader.class, dataLoader);
	}

	@Test
	public void loadBarsToCacheTest() throws Exception {
		BarSize barSize = BarSize._10_MINS;
		int barCount = 30;
		Date now = new Date();
		Calendar testBarsStart = Calendar.getInstance();
		testBarsStart.setTime(now);
		testBarsStart.add(Calendar.SECOND, -barCount * barSize.getSecondsInBar());

		DataLoaderEventHandler dataLoaderHandler = WhiteboxImpl.getInternalState(cacheLoader, DataLoaderEventHandler.class);
		Mockito.doAnswer(new Answer<Void>() {
			@Override
			public Void answer(InvocationOnMock invocation) throws Throwable {
				List<com.ib.controller.Bar> bars = BarTestData.createTestBars(testBarsStart.getTime(), barSize, barCount);
				dataLoaderHandler.barsReceived(bars);
				return null;
			}
		}).when(dataLoader).startRequest(Matchers.any(DataLoaderEventHandler.class));

		cacheLoader.loadBarsFromIB(security, now, BarSize._10_MINS, barCount);

		assertEquals(barCount, barCache.getBarGroupCount(security.getKey(), barSize));
	}
}
