package com.g2m.services.tradingservices.entities;

import java.util.Calendar;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.g2m.services.tradingservices.entities.Security.SecurityBuilder;
import com.g2m.services.tradingservices.enums.SecurityType;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Added 7/5/2015.
 * 
 * @author michaelborromeo
 */
@RunWith(MockitoJUnitRunner.class)
public class SecurityTest {
	@Test
	public void equalityTest_ShouldBeEqual() {
		SecurityBuilder builder = new SecurityBuilder();
		builder.setSymbol("ES");
		builder.setExchange("GLOBEX");
		builder.setExpiry(2015, Calendar.JUNE);
		builder.setSecurityType(SecurityType.FUTURE);
		Security security1 = builder.build();
		builder.setLocalSymbol("ASDF");
		builder.setTradingClass("ASDF");
		builder.setMultiplier("ASDF");
		builder.setListingExchange("GLOBEX");
		Security security2 = builder.build();
		assertTrue(security1.equals(security2));
	}

	@Test
	public void equalityTest_ShouldNotBeEqual_Exchange() {
		SecurityBuilder builder = new SecurityBuilder();
		builder.setSymbol("ES");
		builder.setExchange("GLOBEX");
		builder.setExpiry(2015, Calendar.JUNE);
		builder.setSecurityType(SecurityType.FUTURE);
		Security security1 = builder.build();
		builder.setExchange("ASDF");
		Security security2 = builder.build();
		assertFalse(security1.equals(security2));
	}

	@Test
	public void equalityTest_ShouldNotBeEqual_Symbol() {
		SecurityBuilder builder = new SecurityBuilder();
		builder.setSymbol("ES");
		builder.setExchange("GLOBEX");
		builder.setExpiry(2015, Calendar.JUNE);
		builder.setSecurityType(SecurityType.FUTURE);
		Security security1 = builder.build();
		builder.setSymbol("ASDF");
		Security security2 = builder.build();
		assertFalse(security1.equals(security2));
	}

	@Test
	public void equalityTest_ShouldNotBeEqual_Expiry() {
		SecurityBuilder builder = new SecurityBuilder();
		builder.setSymbol("ES");
		builder.setExchange("GLOBEX");
		builder.setExpiry(2015, Calendar.JUNE);
		builder.setSecurityType(SecurityType.FUTURE);
		Security security1 = builder.build();
		builder.setExpiry(2015, Calendar.SEPTEMBER);
		Security security2 = builder.build();
		assertFalse(security1.equals(security2));
	}

	@Test
	public void equalityTest_ShouldNotBeEqual_SecurityType() {
		SecurityBuilder builder = new SecurityBuilder();
		builder.setSymbol("ES");
		builder.setExchange("GLOBEX");
		builder.setExpiry(2015, Calendar.JUNE);
		builder.setSecurityType(SecurityType.FUTURE);
		Security security1 = builder.build();
		builder.setSecurityType(SecurityType.STOCK);
		Security security2 = builder.build();
		assertFalse(security1.equals(security2));
	}
}
