package com.g2m.services.tradingservices.analytics;

import java.util.Calendar;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.reflect.internal.WhiteboxImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.g2m.services.tradingservices.SecurityRegistry;
import com.g2m.services.tradingservices.entities.Position;
import com.g2m.services.tradingservices.entities.Position.PositionBuilder;
import com.g2m.services.tradingservices.entities.Security;
import com.g2m.services.tradingservices.entities.Security.SecurityBuilder;
import com.g2m.services.tradingservices.entities.Tick;
import com.g2m.services.tradingservices.entities.Tick.TickBuilder;
import com.g2m.services.tradingservices.enums.SecurityType;
import com.g2m.services.tradingservices.enums.WinLoss;
import com.g2m.services.tradingservices.utilities.TestContextConfiguration;

import static org.junit.Assert.assertEquals;

/**
 * Added 6/23/2015.
 * 
 * @author Michael Borromeo
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TestContextConfiguration.class)
public class AnalyticsTest {
	@Autowired
	private Analytics analytics;
	private Security security1;
	private Security security2;

	@Before
	public void setup() {
		SecurityBuilder securityBuilder = new SecurityBuilder();
		securityBuilder.setSymbol("ES");
		securityBuilder.setExchange("GLOBEX");
		securityBuilder.setExpiry(2015, Calendar.JUNE);
		securityBuilder.setSecurityType(SecurityType.FUTURE);
		security1 = securityBuilder.build();
		SecurityRegistry.add(security1);
		securityBuilder.setExpiry(2015, Calendar.SEPTEMBER);
		security2 = securityBuilder.build();
		SecurityRegistry.add(security2);
	}

	private Position createPosition(Security security, double price, int quantity) {
		PositionBuilder builder = new PositionBuilder();
		builder.setAccount("12345");
		builder.setSecurity(security);
		builder.setMarketPrice(price);
		builder.setQuantity(quantity);
		return builder.build();
	}

	private Tick createTick(Security security, double price) {
		TickBuilder builder = new TickBuilder();
		builder.setSecurity(security);
		builder.setAskPrice(price);
		builder.setBidPrice(price);
		builder.setDateTime(new Date());
		builder.setLastPrice(price);
		return builder.build();
	}

	@Test
	public void openAndClosePositionsTest() {
		winningPositionTest(security1);
		AggregateAnalytics aggregateAnalytics = analytics.getAggregateAnalytics();
		assertEquals(20, aggregateAnalytics.getProfit(), 0.01);
		// TODO fix when commissions are fixed
		assertEquals(0, aggregateAnalytics.getTotalComissions(), 0.01);
		assertEquals(4, aggregateAnalytics.getTotalTrades());
		assertEquals(1, (int) aggregateAnalytics.getWinLossCount().get(WinLoss.WIN));
		assertEquals(0, (int) aggregateAnalytics.getWinLossCount().get(WinLoss.LOSS));

		losingPositionTest(security1);
		aggregateAnalytics = analytics.getAggregateAnalytics();
		assertEquals(5, aggregateAnalytics.getProfit(), 0.01);
		// TODO fix when commissions are fixed
		assertEquals(0, aggregateAnalytics.getTotalComissions(), 0.01);
		assertEquals(8, aggregateAnalytics.getTotalTrades());
		assertEquals(1, (int) aggregateAnalytics.getWinLossCount().get(WinLoss.WIN));
		assertEquals(1, (int) aggregateAnalytics.getWinLossCount().get(WinLoss.LOSS));

		winningPositionTest(security2);
		aggregateAnalytics = analytics.getAggregateAnalytics();
		assertEquals(25, aggregateAnalytics.getProfit(), 0.01);
		// TODO fix when commissions are fixed
		assertEquals(0, aggregateAnalytics.getTotalComissions(), 0.01);
		assertEquals(12, aggregateAnalytics.getTotalTrades());
		assertEquals(2, (int) aggregateAnalytics.getWinLossCount().get(WinLoss.WIN));
		assertEquals(1, (int) aggregateAnalytics.getWinLossCount().get(WinLoss.LOSS));

		losingPositionTest(security2);
		aggregateAnalytics = analytics.getAggregateAnalytics();
		assertEquals(10, aggregateAnalytics.getProfit(), 0.01);
		// TODO fix when commissions are fixed
		assertEquals(0, aggregateAnalytics.getTotalComissions(), 0.01);
		assertEquals(16, aggregateAnalytics.getTotalTrades());
		assertEquals(2, (int) aggregateAnalytics.getWinLossCount().get(WinLoss.WIN));
		assertEquals(2, (int) aggregateAnalytics.getWinLossCount().get(WinLoss.LOSS));
	}

	private void winningPositionTest(Security security) {
		// open position
		analytics.updateFromPosition(createPosition(security, 10, 1));
		PositionAnalytics positionAnalytics = analytics.getPositionAnalytics(security.getKey());
		assertEquals(1, positionAnalytics.getTradeCount());
		assertEquals(0, positionAnalytics.getProfit(), 0.01);
		assertEquals(1, (int) WhiteboxImpl.getInternalState(positionAnalytics, "quantity"));
		assertEquals(WinLoss.UNKNOWN, positionAnalytics.getWinLoss());

		// tick with higher price
		analytics.updateFromTick(createTick(security, 15));
		positionAnalytics = analytics.getPositionAnalytics(security.getKey());
		assertEquals(5, positionAnalytics.getMaxDrawUp(), 0.01);
		assertEquals(0, positionAnalytics.getMaxDrawDown(), 0.01);

		// increase position
		analytics.updateFromPosition(createPosition(security, 15, 2));
		positionAnalytics = analytics.getPositionAnalytics(security.getKey());
		assertEquals(2, positionAnalytics.getTradeCount());
		assertEquals(5, positionAnalytics.getProfit(), 0.01);
		assertEquals(2, (int) WhiteboxImpl.getInternalState(positionAnalytics, "quantity"));
		assertEquals(WinLoss.UNKNOWN, positionAnalytics.getWinLoss());

		// tick with higher price
		analytics.updateFromTick(createTick(security, 20));
		positionAnalytics = analytics.getPositionAnalytics(security.getKey());
		assertEquals(10, positionAnalytics.getMaxDrawUp(), 0.01);
		assertEquals(0, positionAnalytics.getMaxDrawDown(), 0.01);

		// tick with lower price
		analytics.updateFromTick(createTick(security, 5));
		positionAnalytics = analytics.getPositionAnalytics(security.getKey());
		assertEquals(10, positionAnalytics.getMaxDrawUp(), 0.01);
		assertEquals(15, positionAnalytics.getMaxDrawDown(), 0.01);

		// reduce position
		analytics.updateFromPosition(createPosition(security, 20, 1));
		positionAnalytics = analytics.getPositionAnalytics(security.getKey());
		assertEquals(3, positionAnalytics.getTradeCount());
		assertEquals(15, positionAnalytics.getProfit(), 0.01);
		assertEquals(1, (int) WhiteboxImpl.getInternalState(positionAnalytics, "quantity"));
		assertEquals(WinLoss.UNKNOWN, positionAnalytics.getWinLoss());

		// tick with higher price
		analytics.updateFromTick(createTick(security, 25));
		positionAnalytics = analytics.getPositionAnalytics(security.getKey());
		assertEquals(20, positionAnalytics.getMaxDrawUp(), 0.01);
		assertEquals(15, positionAnalytics.getMaxDrawDown(), 0.01);

		// close the position
		analytics.updateFromPosition(createPosition(security, 25, 0));
		positionAnalytics = analytics.getPositionAnalytics(security.getKey());
		assertEquals(4, positionAnalytics.getTradeCount());
		assertEquals(20, positionAnalytics.getProfit(), 0.01);
		assertEquals(0, (int) WhiteboxImpl.getInternalState(positionAnalytics, "quantity"));
		assertEquals(WinLoss.WIN, positionAnalytics.getWinLoss());
	}

	private void losingPositionTest(Security security) {
		// open position
		analytics.updateFromPosition(createPosition(security, 10, 1));
		PositionAnalytics positionAnalytics = analytics.getPositionAnalytics(security.getKey());
		assertEquals(1, positionAnalytics.getTradeCount());
		assertEquals(0, positionAnalytics.getProfit(), 0.01);
		assertEquals(1, (int) WhiteboxImpl.getInternalState(positionAnalytics, "quantity"));
		assertEquals(WinLoss.UNKNOWN, positionAnalytics.getWinLoss());

		// tick with higher price
		analytics.updateFromTick(createTick(security, 15));
		positionAnalytics = analytics.getPositionAnalytics(security.getKey());
		assertEquals(5, positionAnalytics.getMaxDrawUp(), 0.01);
		assertEquals(0, positionAnalytics.getMaxDrawDown(), 0.01);

		// increase position
		analytics.updateFromPosition(createPosition(security, 15, 2));
		positionAnalytics = analytics.getPositionAnalytics(security.getKey());
		assertEquals(2, positionAnalytics.getTradeCount());
		assertEquals(5, positionAnalytics.getProfit(), 0.01);
		assertEquals(2, (int) WhiteboxImpl.getInternalState(positionAnalytics, "quantity"));
		assertEquals(WinLoss.UNKNOWN, positionAnalytics.getWinLoss());

		// tick with lower price
		analytics.updateFromTick(createTick(security, 5));
		positionAnalytics = analytics.getPositionAnalytics(security.getKey());
		assertEquals(5, positionAnalytics.getMaxDrawUp(), 0.01);
		assertEquals(10, positionAnalytics.getMaxDrawDown(), 0.01);

		// reduce position
		analytics.updateFromPosition(createPosition(security, 5, 1));
		positionAnalytics = analytics.getPositionAnalytics(security.getKey());
		assertEquals(3, positionAnalytics.getTradeCount());
		assertEquals(-15, positionAnalytics.getProfit(), 0.01);
		assertEquals(1, (int) WhiteboxImpl.getInternalState(positionAnalytics, "quantity"));
		assertEquals(WinLoss.UNKNOWN, positionAnalytics.getWinLoss());

		// close the position
		analytics.updateFromPosition(createPosition(security, 5, 0));
		positionAnalytics = analytics.getPositionAnalytics(security.getKey());
		assertEquals(4, positionAnalytics.getTradeCount());
		assertEquals(-15, positionAnalytics.getProfit(), 0.01);
		assertEquals(0, (int) WhiteboxImpl.getInternalState(positionAnalytics, "quantity"));
		assertEquals(WinLoss.LOSS, positionAnalytics.getWinLoss());
	}
}
