package com.g2m.services.tradingservices.backtest;

import java.util.Calendar;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.g2m.services.tradingservices.SecurityRegistry;
import com.g2m.services.tradingservices.caches.TickCache;
import com.g2m.services.tradingservices.entities.Position.PositionBuilder;
import com.g2m.services.tradingservices.entities.Security;
import com.g2m.services.tradingservices.entities.Security.SecurityBuilder;
import com.g2m.services.tradingservices.entities.Tick.TickBuilder;
import com.g2m.services.tradingservices.entities.orders.MarketOrder;
import com.g2m.services.tradingservices.enums.OrderAction;
import com.g2m.services.tradingservices.enums.OrderStatusReason;
import com.g2m.services.tradingservices.enums.SecurityType;
import com.g2m.services.tradingservices.utilities.TestContextConfiguration;

import static org.junit.Assert.assertEquals;

/**
 * Added 5/18/2015.
 * 
 * @author michaelborromeo
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TestContextConfiguration.class)
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class BacktestAccountTest {
	@Autowired
	private BacktestAccount account;
	@Autowired
	private TickCache tickCache;
	private Security security;
	private static final double TICK_PRICE = 15;

	@Before
	public void setup() {
		SecurityBuilder securityBuilder = new SecurityBuilder();
		securityBuilder.setSymbol("ES");
		securityBuilder.setExchange("GLOBEX");
		securityBuilder.setExpiry(2015, Calendar.JUNE);
		securityBuilder.setSecurityType(SecurityType.FUTURE);
		security = securityBuilder.build();
		SecurityRegistry.add(security);

		TickBuilder tickBuilder = new TickBuilder();
		tickBuilder.setAskPrice(TICK_PRICE);
		tickBuilder.setBidPrice(TICK_PRICE);
		tickBuilder.setChange(0);
		tickBuilder.setDateTime(new Date());
		tickBuilder.setLastPrice(TICK_PRICE);
		tickBuilder.setOpenInterest(0);
		tickBuilder.setSecurity(security);
		tickBuilder.setSettlement(0);
		tickBuilder.setVolume(1);
		tickBuilder.setVolumeAsk(1);
		tickBuilder.setVolumeBid(1);
		tickCache.save(tickBuilder.build());

		PositionBuilder builder = new PositionBuilder();
		builder.setAccount("12345");
		builder.setAverageCost(TICK_PRICE);
		builder.setMarketPrice(TICK_PRICE);
		builder.setMarketValue(TICK_PRICE);
		builder.setQuantity(1);
		builder.setRealizedPnl(0);
		builder.setSecurity(security);
		builder.setUnrealizedPnl(0);
		account.getPositions().put(security.getKey(), builder.build());
	}

	@Test
	public void canFillOrderTest_BuyOkToFill() {
		MarketOrder order = new MarketOrder(OrderAction.BUY, 1, security.getKey());
		account.setupStartingFunds(TICK_PRICE);
		OrderStatusReason reason = account.canFillOrder(order);
		assertEquals(OrderStatusReason.OK_TO_FILL, reason);
	}

	@Test
	public void canFillOrderTest_BuyInsufficientFunds() {
		MarketOrder order = new MarketOrder(OrderAction.BUY, 1, security.getKey());
		account.setupStartingFunds(TICK_PRICE - 1);
		OrderStatusReason reason = account.canFillOrder(order);
		assertEquals(OrderStatusReason.INSUFFICIENT_FUNDS, reason);
	}

	@Test
	public void canFillOrderTest_SellOkToFill() {
		MarketOrder order = new MarketOrder(OrderAction.SELL, 1, security.getKey());
		account.setupStartingFunds(0);
		OrderStatusReason reason = account.canFillOrder(order);
		assertEquals(OrderStatusReason.OK_TO_FILL, reason);
	}

	@Test
	public void canFillOrderTest_SellInsufficientPosition() {
		MarketOrder order = new MarketOrder(OrderAction.SELL, 2, security.getKey());
		account.setupStartingFunds(0);
		OrderStatusReason reason = account.canFillOrder(order);
		assertEquals(OrderStatusReason.INSUFFICIENT_POSITION, reason);
	}

	@Test
	public void updateAccountFromOrderTest_Buy() {
		MarketOrder order = new MarketOrder(OrderAction.BUY, 1, security.getKey());
		account.setupStartingFunds(TICK_PRICE);
		OrderStatusReason reason = account.canFillOrder(order);
		assertEquals(OrderStatusReason.OK_TO_FILL, reason);
		account.updateAccountFromOrder(order);
		assertEquals(2, account.getPositions().get(security.getKey()).getQuantity());
		assertEquals(2 * TICK_PRICE, account.getPositions().get(security.getKey()).getMarketValue(), 0.01);
		assertEquals(0, account.getAvailableFunds(), 0.01);
	}

	@Test
	public void updateAccountFromOrderTest_Sell() {
		account.setupStartingFunds(0);
		MarketOrder order = new MarketOrder(OrderAction.SELL, 1, security.getKey());
		OrderStatusReason reason = account.canFillOrder(order);
		assertEquals(OrderStatusReason.OK_TO_FILL, reason);
		account.updateAccountFromOrder(order);
		assertEquals(0, account.getPositions().get(security.getKey()).getQuantity());
		assertEquals(0, account.getPositions().get(security.getKey()).getMarketValue(), 0.01);
		assertEquals(TICK_PRICE, account.getAvailableFunds(), 0.01);
	}
}
