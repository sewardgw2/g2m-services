package com.g2m.services.tradingservices.testdata;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import com.g2m.services.tradingservices.entities.Security;
import com.g2m.services.tradingservices.entities.Tick;
import com.g2m.services.tradingservices.entities.Tick.TickBuilder;

/**
 * Added 5/14/2015.
 * 
 * @author Michael Borromeo
 */
public class TickTestData {
	public static final Date START_DATETIME = new GregorianCalendar(2014, 1, 1, 0, 0, 0).getTime();

	/**
	 * The test ticks created here will have their values set to the sequence number in which they
	 * come. E.g. The second tick generated will have its open/close/high/low values set to 2.
	 */
	public static List<Tick> createTestTicks(Security security, int count) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(START_DATETIME);
		List<Tick> ticks = new ArrayList<Tick>();
		TickBuilder tickBuilder = new TickBuilder();
		for (int i = 1; i <= count; i++) {
			tickBuilder.setAskPrice(i);
			tickBuilder.setBidPrice(i);
			tickBuilder.setChange(0);
			tickBuilder.setDateTime(calendar.getTime());
			tickBuilder.setLastPrice(i);
			tickBuilder.setOpenInterest(0);
			tickBuilder.setSecurity(security);
			tickBuilder.setVolume(i);
			tickBuilder.setVolume(i);
			tickBuilder.setVolumeBid(i);
			ticks.add(tickBuilder.build());
			calendar.add(Calendar.MILLISECOND, 250);
		}
		return ticks;
	}
}
