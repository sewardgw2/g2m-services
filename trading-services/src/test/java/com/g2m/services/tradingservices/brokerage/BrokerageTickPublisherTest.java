package com.g2m.services.tradingservices.brokerage;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.g2m.services.tradingservices.TickSubscriber;
import com.g2m.services.tradingservices.entities.PendingTick;
import com.g2m.services.tradingservices.entities.Security;
import com.g2m.services.tradingservices.entities.Tick;
import com.g2m.services.tradingservices.entities.Tick.TickBuilder;
import com.g2m.services.tradingservices.utilities.SecurityUtility;
import com.g2m.services.tradingservices.utilities.TestContextConfiguration;

import static org.junit.Assert.assertEquals;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Added 4/19/2015.
 * 
 * @author Michael Borromeo
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TestContextConfiguration.class)
public class BrokerageTickPublisherTest {
	@Mock
	private BrokerageMarketData marketData;
	@Autowired
	@InjectMocks
	private BrokerageTickPublisher tickPublisher;
	private TickSubscriber tickSubscriber;
	private Map<Security, List<Tick>> inputTicks;
	private List<Tick> resultingTicks;
	private List<Iterator<Tick>> tickIterators;
	private List<Security> securities;
	private static final int TICKS_TO_TEST = 20;

	public BrokerageTickPublisherTest() {
		securities = SecurityUtility.createAndRegisterSecurities("GOOG", "MSFT");
		tickSubscriber = new TickSubscriber() {
			@Override
			public void tickReceived(Tick tick) {
				System.out.println("Resulting Tick: " + tick.toString());
				resultingTicks.add(tick);
			}
		};
	}

	@Before
	public void setup() {
		initMocks(this);
		prepareTicks();
	}

	private void prepareTicks() {
		resultingTicks = new ArrayList<Tick>();
		inputTicks = new HashMap<Security, List<Tick>>();
		tickIterators = new ArrayList<Iterator<Tick>>();
		for (Security security : securities) {
			tickPublisher.addTickSubscriber(security.getKey(), tickSubscriber);
			List<Tick> ticks = createTicks(TICKS_TO_TEST, security);
			inputTicks.put(security, ticks);
			tickIterators.add(ticks.iterator());
		}
	}

	@Test
	public void testBrokerageTickPublisher() {
		when(marketData.getPendingTickUpdates()).then(new Answer<List<PendingTick>>() {
			@Override
			public List<PendingTick> answer(InvocationOnMock invocation) throws Throwable {
				return getNextPendingTicks();
			}
		});
		tickPublisher.startPublishing();
		sleepWhileTickPublisherConsumesTicks();
		tickPublisher.stopPublishing();

		// assert that the correct # ticks are coming though
		assertEquals("Number of ticks published should be " + TICKS_TO_TEST * securities.size(),
				TICKS_TO_TEST * securities.size(), resultingTicks.size());
	}

	private void sleepWhileTickPublisherConsumesTicks() {
		boolean moreTicks = true;
		while (moreTicks) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
				break;
			}
			moreTicks = false;
			for (Iterator<Tick> iterator : tickIterators) {
				if (iterator.hasNext()) {
					moreTicks = true;
				}
			}
		}
	}

	private List<PendingTick> getNextPendingTicks() {
		List<PendingTick> pendingTicks = new ArrayList<PendingTick>();
		for (Iterator<Tick> iterator : tickIterators) {
			if (iterator.hasNext()) {
				PendingTick pendingTick = mock(PendingTick.class);
				when(pendingTick.createTick()).thenReturn(iterator.next());
				pendingTicks.add(pendingTick);
			}
		}
		return pendingTicks;
	}

	private List<Tick> createTicks(int count, Security security) {
		List<Tick> ticks = new ArrayList<Tick>();
		TickBuilder builder = new TickBuilder();
		builder.setSecurity(security);
		Date dateTime = new Date(System.currentTimeMillis());
		builder.setDateTime(dateTime);
		for (int i = 0; i < count; i++) {
			builder.setDateTime(dateTime);
			dateTime = new Date(dateTime.getTime() + 1000);
			ticks.add(builder.build());
		}

		return ticks;
	}
}
