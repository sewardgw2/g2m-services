package com.g2m.services.tradingservices.brokerage;

import java.util.Arrays;
import java.util.Calendar;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.reflect.internal.WhiteboxImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.g2m.services.tradingservices.SecurityRegistry;
import com.g2m.services.tradingservices.brokerage.BrokerageAccount.AccountHandler;
import com.g2m.services.tradingservices.brokerage.BrokerageTrader.AllOrdersHandler;
import com.g2m.services.tradingservices.brokerage.mappers.BrokerageSecurityMapper;
import com.g2m.services.tradingservices.entities.Security;
import com.g2m.services.tradingservices.entities.Security.SecurityBuilder;
import com.g2m.services.tradingservices.entities.orders.MarketOrder;
import com.g2m.services.tradingservices.entities.orders.Order;
import com.g2m.services.tradingservices.enums.OrderAction;
import com.g2m.services.tradingservices.enums.OrderStatus;
import com.g2m.services.tradingservices.enums.SecurityType;
import com.g2m.services.tradingservices.utilities.TestContextConfiguration;
import com.ib.client.OrderState;
import com.ib.controller.ApiController;
import com.ib.controller.ApiController.IOrderHandler;
import com.ib.controller.NewContract;
import com.ib.controller.NewOrder;
import com.ib.controller.NewOrderState;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Added 5/9/2015.
 * 
 * @author Michael Borromeo
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TestContextConfiguration.class)
public class BrokerageTraderTest {
	@Mock
	private BrokerageConnection connection;
	@Mock
	private ApiController apiController;
	@InjectMocks
	@Autowired
	private BrokerageAccount account;
	@InjectMocks
	@Autowired
	private BrokerageTrader trader;
	private Order order;

	@Before
	public void setup() throws Exception {
		initMocks(this);
		Mockito.when(connection.getAccountCodes()).thenReturn(Arrays.asList("account123"));
		Mockito.when(connection.getApiController()).thenReturn(apiController);
		SecurityRegistry.clear();
		account.subscribeToUpdates();
		SecurityBuilder securityBuilder = new SecurityBuilder();
		securityBuilder.setSymbol("ES");
		securityBuilder.setExchange("GLOBEX");
		securityBuilder.setExpiry(2015, Calendar.JUNE);
		securityBuilder.setSecurityType(SecurityType.FUTURE);
		Security security = securityBuilder.build();
		SecurityRegistry.add(security);
		order = new MarketOrder(OrderAction.BUY, 1, security.getKey());

		WhiteboxImpl.invokeMethod(trader, BrokerageTrader.class, "initializeMaps");
	}

	@Test
	public void submitOrderTest() {
		long permId = 123;
		Mockito.doAnswer(new Answer<Void>() {
			com.ib.controller.Position position = createPositionFromOrder(order, 0);

			@Override
			public Void answer(InvocationOnMock invocation) throws Throwable {
				// increment the position and pass it to the account handler
				position = createPositionFromOrder(order, position.position() + 1);
				AccountHandler accountHandler = WhiteboxImpl.getInternalState(account, "accountHandler");
				accountHandler.updatePortfolio(position);

				// create the orderstate/status and send it to the order handler
				OrderState ibOrderState = new OrderState();
				ibOrderState.m_status = "Filled";
				NewOrderState orderState = new NewOrderState(ibOrderState);
				BrokerageOrderStates brokerageOrderStates = (BrokerageOrderStates) trader.getOrders(order.getSecurityKey())
						.get(trader.getOrders(order.getSecurityKey()).size() - 1).getOrderStates();
				brokerageOrderStates.getOrderHandler().orderState(orderState);
				brokerageOrderStates.getOrderHandler().orderStatus(com.ib.controller.OrderStatus.Filled,
						order.getQuantity(), 0, 150, permId, 0, 150, 0, "");

				return null;
			}
		}).when(apiController).placeOrModifyOrder(any(NewContract.class), any(NewOrder.class), any(IOrderHandler.class));
		trader.submitOrder(order);

		com.g2m.services.tradingservices.entities.orders.OrderState brokerageOrderState = (com.g2m.services.tradingservices.entities.orders.OrderState) trader
				.getOrders(order.getSecurityKey()).get(trader.getOrders(order.getSecurityKey()).size() - 1)
				.getLatestOrderState();

		// check that the order was placed and is in the orders list
		verify(apiController, times(1)).placeOrModifyOrder(any(NewContract.class), any(NewOrder.class),
				any(IOrderHandler.class));
		assertTrue(1 == trader.getOrders(order.getSecurityKey()).size());

		// check that the order was filled
		assertEquals(OrderStatus.FILLED, brokerageOrderState.getOrderStatus());

		// check that the perm id passed is set on the order
		assertEquals(permId, brokerageOrderState.getId());

		// TODO check the position
	}

	private com.ib.controller.Position createPositionFromOrder(Order order, int position) {
		return new com.ib.controller.Position(BrokerageSecurityMapper.createContract(SecurityRegistry.get(order
				.getSecurityKey())), "12345", position, 0, 0, 0, 0, 0);
	}

	@Test
	public void cancelOrderTest() {
		long permId = 123;
		int orderId = 11;
		int filled = 50;
		int remaining = 0;
		double lastFillPrice = 150;
		double averageFillPrice = 150;
		int clientId = 0;
		String whyHeld = "";
		Mockito.doAnswer(new Answer<Void>() {
			com.ib.controller.Position position = createPositionFromOrder(order, 0);

			@Override
			public Void answer(InvocationOnMock invocation) throws Throwable {
				// increment the position and pass it to the account handler
				position = createPositionFromOrder(order, position.position() + 1);
				AccountHandler accountHandler = WhiteboxImpl.getInternalState(account, "accountHandler");
				accountHandler.updatePortfolio(position);

				// create the orderstate/status and send it to the order handler
				OrderState ibOrderState = new OrderState();
				ibOrderState.m_status = "Submitted";
				NewOrderState orderState = new NewOrderState(ibOrderState);
				BrokerageOrderStates brokerageOrderStates = (BrokerageOrderStates) trader.getOrders(order.getSecurityKey())
						.get(trader.getOrders(order.getSecurityKey()).size() - 1).getOrderStates();
				brokerageOrderStates.getOrderHandler().orderState(orderState);
				brokerageOrderStates.getOrderHandler().orderStatus(com.ib.controller.OrderStatus.Submitted,
						order.getQuantity(), 0, 150, permId, 0, 150, 0, "");

				AllOrdersHandler allOrdersHandler = WhiteboxImpl.getInternalState(trader, "allOrdersHandler");
				allOrdersHandler.orderStatus(orderId, orderState.status(), filled, remaining, averageFillPrice, permId, 0,
						lastFillPrice, clientId, whyHeld);

				return null;
			}
		}).when(apiController).placeOrModifyOrder(any(NewContract.class), any(NewOrder.class), any(IOrderHandler.class));
		Mockito.doAnswer(new Answer<Void>() {
			@Override
			public Void answer(InvocationOnMock invocation) throws Throwable {

				// create the orderstate/status and send it to the order handler
				OrderState ibOrderState = new OrderState();
				ibOrderState.m_status = "Cancelled";
				NewOrderState orderState = new NewOrderState(ibOrderState);
				BrokerageOrderStates brokerageOrderStates = (BrokerageOrderStates) trader.getOrders(order.getSecurityKey())
						.get(trader.getOrders(order.getSecurityKey()).size() - 1).getOrderStates();
				brokerageOrderStates.getOrderHandler().orderState(orderState);
				brokerageOrderStates.getOrderHandler().orderStatus(orderState.status(), filled, remaining, averageFillPrice,
						permId, 0, lastFillPrice, clientId, whyHeld);

				return null;
			}
		}).when(apiController).cancelOrder(orderId);
		trader.submitOrder(order);

		com.g2m.services.tradingservices.entities.orders.OrderState brokerageOrderState = (com.g2m.services.tradingservices.entities.orders.OrderState) trader
				.getOrders(order.getSecurityKey()).get(trader.getOrders(order.getSecurityKey()).size() - 1)
				.getLatestOrderState();

		// check that the order was submitted
		assertEquals(OrderStatus.SUBMITTED, brokerageOrderState.getOrderStatus());

		// cancel the order and check that it was cancelled
		Order orderToCancel = trader.getOrders(order.getSecurityKey()).get(
				trader.getOrders(order.getSecurityKey()).size() - 1);
		trader.cancelOrder(orderToCancel.getKey());
		brokerageOrderState = (com.g2m.services.tradingservices.entities.orders.OrderState) trader
				.getOrders(order.getSecurityKey()).get(trader.getOrders(order.getSecurityKey()).size() - 1)
				.getLatestOrderState();
		assertEquals(OrderStatus.CANCELLED, brokerageOrderState.getOrderStatus());

		// check that the order was placed, cancelled and is in the orders list
		verify(apiController, times(1)).placeOrModifyOrder(any(NewContract.class), any(NewOrder.class),
				any(IOrderHandler.class));
		verify(apiController, times(1)).cancelOrder(anyInt());
		assertTrue(1 == trader.getOrders(order.getSecurityKey()).size());

		// check that the perm id passed is set on the order
		assertEquals(permId, brokerageOrderState.getId());
	}
}
