package com.g2m.services.tradingservices.caches;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.g2m.services.tradingservices.entities.Bar;
import com.g2m.services.tradingservices.entities.Security;
import com.g2m.services.tradingservices.entities.Security.SecurityBuilder;
import com.g2m.services.tradingservices.enums.BarSize;
import com.g2m.services.tradingservices.enums.SecurityType;
import com.g2m.services.tradingservices.testdata.BarTestData;
import com.g2m.services.tradingservices.utilities.TestContextConfiguration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Added 3/1/2015.
 * 
 * @author Michael Borromeo
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TestContextConfiguration.class)
public class BarCacheTest {
	@Autowired
	private BarCache barCache;
	private Security security1;
	private Security security2;
	private Security security3;

	@Before
	public void setup() {
		barCache.clear();
		setupSecurities();
	}

	private void setupSecurities() {
		SecurityBuilder securityBuilder = new SecurityBuilder();
		securityBuilder.setSymbol("ES");
		securityBuilder.setExchange("GLOBEX");
		securityBuilder.setExpiry(2015, Calendar.JUNE);
		securityBuilder.setSecurityType(SecurityType.FUTURE);
		security1 = securityBuilder.build();
		securityBuilder.setExpiry(2015, Calendar.SEPTEMBER);
		security2 = securityBuilder.build();
		securityBuilder.setExpiry(2015, Calendar.DECEMBER);
		security3 = securityBuilder.build();
	}

	/**
	 * Setup multiple bar lists with different security.getKey()/barsize combos in the cache then
	 * test all the cache functionality
	 */
	@Test
	public void getValuesTest() {
		// need to put enough bars into the cache to the point where it has to prune them
		int totalBarCount = barCache.getBarGroupMaxSize() * 2;
		Security[] securities = { security1, security2, security3 };
		BarSize[] barSizes = { BarSize._10_MINS, BarSize._1_MIN, BarSize._1_DAY };
		List<List<Bar>> barLists = new ArrayList<List<Bar>>();

		createBarsAndAddToCache(securities, barSizes, barLists, totalBarCount);

		for (int i = 0; i < securities.length; i++) {
			// check to see that the bar cache has been pruned by getting all the bars in the cache
			assertEquals("The bar cache for " + securities[i].toString() + " should be pruned down to its max size of "
					+ barCache.getBarGroupMaxSize(), barCache.getBarGroupMaxSize(),
					barCache.getBarGroupCount(securities[i].getKey(), barSizes[i]));

			/*
			 * Since the bar lists will have size=totalBarCount but the cache will only have
			 * size=getBarCacheMaxSize() we need to create a sublist of bars that expected to be in
			 * the cache
			 */
			List<Bar> barsExpectedInCache = barLists.get(i).subList(totalBarCount - barCache.getBarGroupMaxSize(),
					barLists.get(i).size());

			getValuesTest_RetrievalTest(securities[i], barSizes[i], barCache.getBarGroupMaxSize(), barsExpectedInCache);
			getValuesTest_DateTimeTest(securities[i], barSizes[i], barCache.getBarGroupMaxSize(), barsExpectedInCache);
		}
	}

	private void createBarsAndAddToCache(Security[] securities, BarSize[] barSizes, List<List<Bar>> barLists,
			int totalBarCount) {
		for (int i = 0; i < securities.length; i++) {
			// request double the cache max size so we can check if pruning is happening
			barLists.add(BarTestData.createTestBars(securities[i], barSizes[i], totalBarCount));
			for (Bar bar : barLists.get(i)) {
				barCache.save(bar);
			}
		}
	}

	private void getValuesTest_DateTimeTest(Security security, BarSize barSize, int count, List<Bar> barsExpectedInCache) {
		String barListHash = "barSize=" + barSize.toString() + "&" + security.toString() + "&count=" + count;

		// check the ordering of the bars by comparing dateTime values
		List<Bar> cacheValues;
		cacheValues = barCache.getBars(security.getKey(), barSize, barsExpectedInCache.get(1).getDateTime(), 2);
		assertEquals("Expected two values to be returned from the cache (" + barListHash + ")", 2, cacheValues.size());
		assertEquals("Expected the dateTime for the bar to be " + barsExpectedInCache.get(0).getDateTime().toString() + " ("
				+ barListHash + ")", barsExpectedInCache.get(0).getDateTime(), cacheValues.get(0).getDateTime());
		assertTrue(cacheValues.get(1).getDateTime().after(cacheValues.get(0).getDateTime()));
	}

	private void getValuesTest_RetrievalTest(Security security, BarSize barSize, int count, List<Bar> barsExpectedInCache) {
		String barListHash = "barSize=" + barSize.toString() + "&" + security.toString() + "&count=" + count;

		// request for bars that go earlier than the beginning of the cache
		List<Bar> cacheValues;
		cacheValues = barCache.getBars(security.getKey(), barSize, barsExpectedInCache.get(2).getDateTime(), 10);
		assertTrue("Expected an empty list due to insufficient bars in cache (" + barListHash + ")", cacheValues.isEmpty());

		// request for bars that go earlier the beginning of the cache
		cacheValues = barCache.getBars(security.getKey(), barSize, barsExpectedInCache.get(8).getDateTime(), 10);
		assertTrue("Expected an empty list due to insufficient bars in cache (" + barListHash + ")", cacheValues.isEmpty());

		// request for existing bars in the cache
		cacheValues = barCache.getBars(security.getKey(), barSize, barsExpectedInCache.get(9).getDateTime(), 1);
		assertEquals("Expected 1 cached value to be returned (" + barListHash + ")", 1, cacheValues.size());

		// request for existing bars in the cache
		cacheValues = barCache.getBars(security.getKey(), barSize, barsExpectedInCache.get(9).getDateTime(), 10);
		assertEquals("Expected 10 cached values to be returned (" + barListHash + ")", 10, cacheValues.size());

		// request for existing bars in the cache
		cacheValues = barCache.getBars(security.getKey(), barSize, barsExpectedInCache.get(count - 1).getDateTime(), 10);
		assertEquals("Expected 10 cached values to be returned (" + barListHash + ")", 10, cacheValues.size());

		// request for all existing bars in the cache
		cacheValues = barCache.getBars(security.getKey(), barSize, barsExpectedInCache.get(count - 1).getDateTime(), count);
		assertEquals("Expected " + count + " cached values to be returned (" + barListHash + ")", count, cacheValues.size());

		// request for more bars (1 more) than exist in the cache
		cacheValues = barCache.getBars(security.getKey(), barSize, barsExpectedInCache.get(count - 1).getDateTime(),
				count + 1);
		assertTrue("Expected an empty list due to insufficient bars in cache (" + barListHash + ")", cacheValues.isEmpty());

		Calendar calendar = Calendar.getInstance();

		// request for bars that are 1 second older than the oldest bar in the cache
		calendar.setTime(barsExpectedInCache.get(0).getDateTime());
		calendar.add(Calendar.SECOND, -1);
		cacheValues = barCache.getBars(security.getKey(), barSize, calendar.getTime(), 10);
		assertTrue("Expected an empty list due to insufficient bars in cache (" + barListHash + ")", cacheValues.isEmpty());

		// request for bars that exist in the cache and that are 1 second old
		calendar.setTime(barsExpectedInCache.get(count - 1).getDateTime());
		calendar.add(Calendar.SECOND, 1);
		cacheValues = barCache.getBars(security.getKey(), barSize, calendar.getTime(), 10);
		assertEquals("Expected 10 cached values to be returned (" + barListHash + ")", 10, cacheValues.size());

		/*
		 * This will work now but at some point bars in the cache could be made to expire if their
		 * timestamp is too far before the requested datetime
		 */

		// request for bars that exist in the cache but are very old
		calendar.add(Calendar.SECOND, 1000000);
		cacheValues = barCache.getBars(security.getKey(), barSize, calendar.getTime(), 10);
		assertEquals("Expected 10 cached values to be returned (" + barListHash + ")", 10, cacheValues.size());

		// request for bars with barsize that doesn't exist in cache
		cacheValues = barCache.getBars(security.getKey(), BarSize._20_MINS, barsExpectedInCache.get(50).getDateTime(), 1);
		assertTrue("Expected an empty list due to invalid barSize (" + barListHash + ")", cacheValues.isEmpty());
	}
}
