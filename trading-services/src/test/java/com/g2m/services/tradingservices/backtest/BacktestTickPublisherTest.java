package com.g2m.services.tradingservices.backtest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.g2m.services.tradingservices.SecurityRegistry;
import com.g2m.services.tradingservices.TickSubscriber;
import com.g2m.services.tradingservices.backtest.BacktestTickPublisher;
import com.g2m.services.tradingservices.entities.Security;
import com.g2m.services.tradingservices.entities.Security.SecurityBuilder;
import com.g2m.services.tradingservices.entities.Tick;
import com.g2m.services.tradingservices.enums.SecurityType;
import com.g2m.services.tradingservices.utilities.TestContextConfiguration;

/**
 * Added 4/16/2015.
 * 
 * @author Michael Borromeo
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TestContextConfiguration.class)
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class BacktestTickPublisherTest {
	@Autowired
	private BacktestTickPublisher testTickPublisher;
	private TickSubscriber tickSubscriber;
	private List<Tick> testTicks;
	private Security security1;
	private Security security2;
	private File security1File;
	private File security2File;
	private File security3File;

	public BacktestTickPublisherTest() {
		SecurityBuilder securityBuilder = new SecurityBuilder();
		securityBuilder.setSymbol("ES");
		securityBuilder.setExchange("GLOBEX");
		securityBuilder.setExpiry(2015, Calendar.JUNE);
		securityBuilder.setSecurityType(SecurityType.FUTURE);
		security1 = securityBuilder.build();
		SecurityRegistry.add(security1);

		securityBuilder.setSymbol("NX");
		securityBuilder.setExchange("SMART");
		security2 = securityBuilder.build();
		SecurityRegistry.add(security2);

		ClassLoader classLoader = getClass().getClassLoader();
		security1File = new File(classLoader.getResource("security1.csv").getFile());
		security2File = new File(classLoader.getResource("security2.csv").getFile());
		security3File = new File(classLoader.getResource("security3.csv").getFile());

		tickSubscriber = new TickSubscriber() {
			@Override
			public void tickReceived(Tick tick) {
				testTicks.add(tick);
			}
		};
	}

	@Before
	public void setup() {
		testTicks = new ArrayList<Tick>();
	}

	@Test
	public void testTickPublisher_TwoSecuritiesOverlappingTicks() {
		testTickPublisher.addTestFile(security1.getKey(), security1File.getAbsolutePath());
		testTickPublisher.addTestFile(security2.getKey(), security2File.getAbsolutePath());
		testTickPublisher.addTickSubscriber(security1.getKey(), tickSubscriber);
		testTickPublisher.addTickSubscriber(security2.getKey(), tickSubscriber);
		testTickPublisher.startPublishing();
		assertTicksAreInOrder();
	}

	@Test
	public void testTickPublisher_TwoSecuritiesNonOverlappingTicks() {
		testTickPublisher.addTestFile(security1.getKey(), security1File.getAbsolutePath());
		testTickPublisher.addTestFile(security2.getKey(), security3File.getAbsolutePath());
		testTickPublisher.addTickSubscriber(security1.getKey(), tickSubscriber);
		testTickPublisher.addTickSubscriber(security2.getKey(), tickSubscriber);
		testTickPublisher.startPublishing();
		assertTicksAreInOrder();
	}

	private void assertTicksAreInOrder() {
		for (int i = 1; i < testTicks.size(); i++) {
			assertTrue(testTicks.get(i).getDateTime().getTime() > testTicks.get(i - 1).getDateTime().getTime());
		}
	}

	@Test
	public void testTickPublisher_ReadFileCorrectly() {
		testTickPublisher.addTestFile(security1.getKey(), security1File.getAbsolutePath());
		testTickPublisher.addTickSubscriber(security1.getKey(), tickSubscriber);
		testTickPublisher.addTickSubscriber(security2.getKey(), tickSubscriber);
		testTickPublisher.startPublishing();
		assertTickValuesAreCorrect();
	}

	/**
	 * Validate the following contents of 'security.csv':
	 * 
	 * dateTime,last,lastDateTime,volBid,volAsk,bid,ask,vol,openIntrest,settlement
	 * 1000000002500,100,1000000002500,1,2,99,101,10,1,777
	 * 1000000003500,200,1000000003500,2,3,199,201,11,2,888
	 * 1000000004500,300,1000000004500,3,4,299,301,12,3,999
	 * 1000000005500,400,1000000005500,4,5,399,401,13,4,555
	 * 1000000006500,500,1000000006500,5,6,499,501,14,5,444
	 */
	private void assertTickValuesAreCorrect() {
		// check the first tick
		assertEquals("Incorrect dateTime", 1000000002500L, testTicks.get(0).getDateTime().getTime());
		assertEquals("Incorrect lastPrice", 100D, testTicks.get(0).getLastPrice(), .01);
		assertEquals("Incorrect volBid", 1, testTicks.get(0).getVolumeBid());
		assertEquals("Incorrect volAsk", 2, testTicks.get(0).getVolumeAsk());
		assertEquals("Incorrect bid", 99D, testTicks.get(0).getBidPrice(), .01);
		assertEquals("Incorrect ask", 101D, testTicks.get(0).getAskPrice(), .01);
		assertEquals("Incorrect vol", 10, testTicks.get(0).getVolume());
		assertEquals("Incorrect openIntrest", 1, testTicks.get(0).getOpenInterest());
		assertEquals("Incorrect settlement", 777D, testTicks.get(0).getSettlement(), .01);

		// check the last tick
		assertEquals("Incorrect dateTime", 1000000006500L, testTicks.get(4).getDateTime().getTime());
		assertEquals("Incorrect lastPrice", 500D, testTicks.get(4).getLastPrice(), .01);
		assertEquals("Incorrect volBid", 5, testTicks.get(4).getVolumeBid());
		assertEquals("Incorrect volAsk", 6, testTicks.get(4).getVolumeAsk());
		assertEquals("Incorrect bid", 499D, testTicks.get(4).getBidPrice(), .01);
		assertEquals("Incorrect ask", 501D, testTicks.get(4).getAskPrice(), .01);
		assertEquals("Incorrect vol", 14, testTicks.get(4).getVolume());
		assertEquals("Incorrect openIntrest", 5, testTicks.get(4).getOpenInterest());
		assertEquals("Incorrect settlement", 444D, testTicks.get(4).getSettlement(), .01);
	}
}
