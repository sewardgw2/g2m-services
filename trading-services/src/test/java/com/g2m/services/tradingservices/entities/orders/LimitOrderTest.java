package com.g2m.services.tradingservices.entities.orders;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;

import com.g2m.services.tradingservices.enums.OrderAction;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Added 5/18/2015.
 * 
 * @author michaelborromeo
 */
@RunWith(PowerMockRunner.class)
public class LimitOrderTest {
	@Test
	public void readyToFillTest_stopLoss() {
		LimitOrder order = new LimitOrder(OrderAction.BUY, 1, null, 10);
		assertFalse(order.isReadyToFill(16));
		assertFalse(order.isReadyToFill(15));
		assertFalse(order.isReadyToFill(14));
		assertFalse(order.isReadyToFill(11));
		assertFalse(order.isReadyToFill(10.01));
		assertTrue(order.isReadyToFill(10));
		assertTrue(order.isReadyToFill(9));
		assertTrue(order.isReadyToFill(11));
		assertTrue(order.isReadyToFill(10));
	}

	@Test
	public void readyToFillTest_stopGain() {
		LimitOrder order = new LimitOrder(OrderAction.SELL, 1, null, 20);
		assertFalse(order.isReadyToFill(14));
		assertFalse(order.isReadyToFill(15));
		assertFalse(order.isReadyToFill(16));
		assertFalse(order.isReadyToFill(19));
		assertTrue(order.isReadyToFill(20));
		assertTrue(order.isReadyToFill(21));
		assertTrue(order.isReadyToFill(19));
	}
}
