package com.g2m.services.tradingservices;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.g2m.services.shared.utilities.DateTimeUtility;
import com.g2m.services.tradingservices.entities.Bar;
import com.g2m.services.tradingservices.entities.Security;
import com.g2m.services.tradingservices.entities.Security.SecurityBuilder;
import com.g2m.services.tradingservices.entities.Tick;
import com.g2m.services.tradingservices.entities.Tick.TickBuilder;
import com.g2m.services.tradingservices.enums.BarSize;
import com.g2m.services.tradingservices.enums.SecurityType;
import com.g2m.services.tradingservices.utilities.TestContextConfiguration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Added 4/11/2015.
 * 
 * @author Michael Borromeo
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TestContextConfiguration.class)
public class BarPublisherTest {
	@Autowired
	private BarPublisher barPublisher;
	private final static long SHORT_TICK_INCREMENT_MILLISECONDS = 250;
	private final static long LONG_TICK_INCREMENT_MILLISECONDS = 60000;
	private Security security;
	private List<Bar> testBars;
	private int milisUntilFirstTick = 1;
	private boolean firstTickSent = false;

	public BarPublisherTest() {
		SecurityBuilder securityBuilder = new SecurityBuilder();
		securityBuilder.setSymbol("ES");
		securityBuilder.setExchange("GLOBEX");
		securityBuilder.setExpiry(2015, Calendar.JUNE);
		securityBuilder.setSecurityType(SecurityType.FUTURE);
		security = securityBuilder.build();
		SecurityRegistry.add(security);
	}

	private void resetBarPublisher() {
		testBars = new ArrayList<Bar>();
		barPublisher.clear();
	}

	@Test
	public void testBarCreation() {
		for (BarSize barSize : BarSize.values()) {
			resetBarPublisher();
			testBarCreation(barSize);
		}
	}

	public void testBarCreation(BarSize barSize) {
		System.out.println("testBarCreation(" + barSize + ")");
		barPublisher.addBarSubscriber(security.getKey(), barSize, new BarSubscriber() {
			@Override
			public void barCreated(Bar bar) {
				testBars.add(bar);
			}
		});

		BarTargetValues bar1TargetValues = new BarTargetValues(100, 50, 75, 85);
		BarTargetValues bar2TargetValues = new BarTargetValues(105, 45, 86, 95);
		BarTargetValues bar3TargetValues = new BarTargetValues(110, 40, 96, 105);

		List<Tick> allTicks = new ArrayList<Tick>();
		List<Tick> bar1Ticks = createTicksForBars(getFirstBarStartTime(barSize), barSize, bar1TargetValues);
		List<Tick> bar2Ticks = createTicksForBars(bar1Ticks, barSize, bar2TargetValues);
		List<Tick> bar3Ticks = createTicksForBars(bar2Ticks, barSize, bar3TargetValues);
		allTicks.addAll(bar1Ticks);
		allTicks.addAll(bar2Ticks);
		allTicks.addAll(bar3Ticks);

		for (Tick tick : allTicks) {
			barPublisher.getTickSubscriber().tickReceived(tick);
		}
		barPublisher.flushBufferedBars();

		assertTrue("3 bars should have been created.", 3 == testBars.size());
		assertBarValues("Bar1", bar1Ticks, testBars.get(0), barSize, bar1TargetValues);
		assertBarValues("Bar2", bar2Ticks, testBars.get(1), barSize, bar2TargetValues);
		assertBarValues("Bar3", bar3Ticks, testBars.get(2), barSize, bar3TargetValues);
	}


	private long getFirstBarStartTime(BarSize barSize) {
		
		// This function is copied / pasted from the BarPublisher class, 
		// is it possible to just call this function in the BarPublisher
		// class directly?
		long currTime = System.currentTimeMillis();
		long milliInDay = 24*60*60*1000;
		long twoHoursInMilliseconds = 2*60*60*1000;
		long remainder;
		long distFromNearestStart;


		remainder = milliInDay - (currTime % milliInDay);
		if (remainder != 0){
			distFromNearestStart = (barSize.getSecondsInBar()*1000) - (remainder % (barSize.getSecondsInBar()*1000));

			// The industry calculates pivot points from 6pm - 6pm so we offset the number of 
			// hours used to calculate the 1 day bar size by 2 hours
			if(barSize == BarSize._1_DAY)
				return currTime - distFromNearestStart - twoHoursInMilliseconds;// - barSize.getSecondsInBar()*1000;
			else
				return currTime - distFromNearestStart;// - barSize.getSecondsInBar()*1000;
			
		} else{
			if(barSize == BarSize._1_DAY)
				return currTime - barSize.getSecondsInBar()*1000 -  twoHoursInMilliseconds;
			else
				return currTime - barSize.getSecondsInBar()*1000;  
		}
	}

	private void assertBarValues(String barName, List<Tick> barTicks, Bar bar, BarSize barSize,
			BarTargetValues barTargetValues) {
		
		// check the bar timestamps
		long expectedBarDateTime = getExpectedBarDateTime(barTicks, barSize);
		assertEquals(barName + " start time should be " + expectedBarDateTime, expectedBarDateTime - milisUntilFirstTick, 
				bar.getDateTime().getTime());

		// check the high, low, open, close values
		assertEquals(barName + " high should be " + barTargetValues.getHigh(), barTargetValues.getHigh(), bar.getHigh(), .01);
		assertEquals(barName + " low should be " + barTargetValues.getLow(), barTargetValues.getLow(), bar.getLow(), .01);
		assertEquals(barName + " open should be " + barTargetValues.getOpen(), barTargetValues.getOpen(), bar.getOpen(), .01);
		assertEquals(barName + " close should be " + barTargetValues.getClose(), barTargetValues.getClose(), bar.getClose(),
				.01);

		// check the volume values (it's 1 per tick)
		assertEquals(barName + " volume should be " + barTicks.size(), barTicks.size(), bar.getVolume());

		// check the security
		assertEquals(barName + " security should be " + security.toString(), security.getKey(), bar.getSecurity().getKey());
	}

	private long getExpectedBarDateTime(List<Tick> barTicks, BarSize barSize) {
		if (BarSize._1_DAY.equals(barSize)) {
			return DateTimeUtility.getMidnightEST(barTicks.get(0).getDateTime()).getTime();
		} else {
			return barTicks.get(0).getDateTime().getTime() + barSize.getSecondsInBar()*1000;
		}
	}

	private List<Tick> createTicksForBars(List<Tick> existingTicks, BarSize barSize, BarTargetValues targetValues) {
		return createTicksForBars(existingTicks.get(existingTicks.size() - 1).getDateTime().getTime()
				+ getIncrement(barSize), barSize, targetValues);
	}

	private long getIncrement(BarSize barSize) {
		if (BarSize._1_DAY.equals(barSize)) {
			return LONG_TICK_INCREMENT_MILLISECONDS;
		} else {
			return SHORT_TICK_INCREMENT_MILLISECONDS;
		}
	}

	private List<Tick> createTicksForBars(long startTimestamp, BarSize barSize, BarTargetValues targetValues) {
		long timestamp = startTimestamp;
		List<Tick> ticks = new ArrayList<Tick>();
		long barEndInMilliseconds = timestamp + (barSize.getSecondsInBar() * 1000);
		while (timestamp < barEndInMilliseconds) {
			double value;

			// ensure the high/low/open/close values are present
			// use open price for everything but the second tick (insert the high), third tick
			// (insert the low) and the last tick (insert the close)
			if (1 == ticks.size()) {
				value = targetValues.getHigh();
			} else if (2 == ticks.size()) {
				value = targetValues.getLow();
			} else if (timestamp + getIncrement(barSize) >= barEndInMilliseconds) {
				value = targetValues.getClose();
			} else {
				value = targetValues.getOpen();
			}

			TickBuilder tickBuilder = new TickBuilder();
			tickBuilder.setSecurity(security);
			tickBuilder.setDateTime(new Date(timestamp+milisUntilFirstTick));
			tickBuilder.setAskPrice(value);
			tickBuilder.setBidPrice(value);
			tickBuilder.setChange(0);
			tickBuilder.setLastPrice(value);
			tickBuilder.setOpenInterest(0);
			tickBuilder.setSettlement(0);
			tickBuilder.setVolume(1);
			tickBuilder.setVolumeAsk(1);
			tickBuilder.setVolumeBid(1);
			ticks.add(tickBuilder.build());
			timestamp += getIncrement(barSize);
		}
		
		firstTickSent = true;
		return ticks;
	}

	private static class BarTargetValues {
		private double high;
		private double low;
		private double open;
		private double close;

		public BarTargetValues(double high, double low, double open, double close) {
			this.high = high;
			this.low = low;
			this.open = open;
			this.close = close;
		}

		public double getHigh() {
			return high;
		}

		public double getLow() {
			return low;
		}

		public double getOpen() {
			return open;
		}

		public double getClose() {
			return close;
		}
	}
}
