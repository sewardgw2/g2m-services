package com.g2m.services.tradingservices.caches;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.g2m.services.tradingservices.entities.Security;
import com.g2m.services.tradingservices.entities.Security.SecurityBuilder;
import com.g2m.services.tradingservices.entities.Tick;
import com.g2m.services.tradingservices.enums.SecurityType;
import com.g2m.services.tradingservices.testdata.TickTestData;
import com.g2m.services.tradingservices.utilities.TestContextConfiguration;

import static org.junit.Assert.assertEquals;

/**
 * Added 5/14/2015.
 * 
 * @author Michael Borromeo
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TestContextConfiguration.class)
public class TickCacheTest {
	@Autowired
	private TickCache tickCache;
	private Security security1;
	private Security security2;

	@Before
	public void setup() {
		tickCache.clear();
		setupSecurities();
	}

	private void setupSecurities() {
		SecurityBuilder securityBuilder = new SecurityBuilder();
		securityBuilder.setSymbol("ES");
		securityBuilder.setExchange("GLOBEX");
		securityBuilder.setExpiry(2015, Calendar.JUNE);
		securityBuilder.setSecurityType(SecurityType.FUTURE);
		security1 = securityBuilder.build();
		securityBuilder.setExpiry(2015, Calendar.SEPTEMBER);
		security2 = securityBuilder.build();
	}

	@Test
	public void getTicksTest() {
		// need to put enough ticks into the cache to the point where it has to prune them
		int totalTickCount = tickCache.getMaxSize() * 2;
		Security[] securities = { security1, security2 };
		List<List<Tick>> tickLists = new ArrayList<List<Tick>>();

		createTicksAndAddToCache(securities, tickLists, totalTickCount);

		for (int i = 0; i < securities.length; i++) {
			/*
			 * Check to see that the tick cache has been pruned by getting all the ticks in the
			 * cache
			 */
			assertEquals(tickCache.getMaxSize(), tickCache.getTickCount(securities[i].getKey()));

			/*
			 * Since the tick lists will have size=totalTickCount but the cache will only have
			 * size=getMaxSize() we need to create a sublist of ticks that expected to be in the
			 * cache
			 */
			List<Tick> ticksExpectedInCache = tickLists.get(i).subList(totalTickCount - tickCache.getMaxSize(),
					tickLists.get(i).size());
			getTicksTest_RetrievalTest(securities[i], tickCache.getMaxSize(), ticksExpectedInCache);
		}
	}

	private void createTicksAndAddToCache(Security[] securities, List<List<Tick>> tickLists, int totalTickCount) {
		for (int i = 0; i < securities.length; i++) {
			// request double the cache max size so we can check if pruning is happening
			tickLists.add(TickTestData.createTestTicks(securities[i], totalTickCount));
			for (Tick tick : tickLists.get(i)) {
				tickCache.save(tick);
			}
		}
	}

	private void getTicksTest_RetrievalTest(Security security, int count, List<Tick> ticksExpectedInCache) {
		// match the ticks in the cache with the ones that are expected
		List<Tick> cachedTicks = tickCache.getTicks(security.getKey(), tickCache.getMaxSize());
		assertEquals(ticksExpectedInCache.size(), cachedTicks.size());
		for (int i = 0; i < ticksExpectedInCache.size(); i++) {
			Tick tickExpected = ticksExpectedInCache.get(i);
			Tick tickCached = cachedTicks.get(i);
			assertEquals(tickExpected.getAskPrice(), tickCached.getAskPrice(), 0.01);
			assertEquals(tickExpected.getBidPrice(), tickCached.getBidPrice(), 0.01);
			assertEquals(tickExpected.getChange(), tickCached.getChange(), 0.01);
			assertEquals(tickExpected.getDateTime(), tickCached.getDateTime());
			assertEquals(tickExpected.getLastPrice(), tickCached.getLastPrice(), 0.01);
			assertEquals(tickExpected.getOpenInterest(), tickCached.getOpenInterest());
			assertEquals(tickExpected.getSecurity().getKey(), tickCached.getSecurity().getKey());
			assertEquals(tickExpected.getSettlement(), tickCached.getSettlement(), 0.01);
			assertEquals(tickExpected.getVolume(), tickCached.getVolume());
			assertEquals(tickExpected.getVolumeAsk(), tickCached.getVolumeAsk());
			assertEquals(tickExpected.getVolumeBid(), tickCached.getVolumeBid());
		}
	}
}
