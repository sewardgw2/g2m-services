package com.g2m.services.tradingservices.backtest;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.reflect.internal.WhiteboxImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.g2m.services.tradingservices.SecurityRegistry;
import com.g2m.services.tradingservices.TickSubscriber;
import com.g2m.services.tradingservices.caches.TickCache;
import com.g2m.services.tradingservices.entities.Position.PositionBuilder;
import com.g2m.services.tradingservices.entities.Security;
import com.g2m.services.tradingservices.entities.Security.SecurityBuilder;
import com.g2m.services.tradingservices.entities.Tick;
import com.g2m.services.tradingservices.entities.Tick.TickBuilder;
import com.g2m.services.tradingservices.entities.orders.LimitOrder;
import com.g2m.services.tradingservices.entities.orders.MarketOrder;
import com.g2m.services.tradingservices.entities.orders.Order;
import com.g2m.services.tradingservices.entities.orders.StopOrder;
import com.g2m.services.tradingservices.enums.OrderAction;
import com.g2m.services.tradingservices.enums.OrderStatus;
import com.g2m.services.tradingservices.enums.OrderStatusReason;
import com.g2m.services.tradingservices.enums.SecurityType;
import com.g2m.services.tradingservices.utilities.TestContextConfiguration;

import static org.junit.Assert.assertEquals;

/**
 * Added 5/18/2015.
 * 
 * @author michaelborromeo
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TestContextConfiguration.class)
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class BacktestTraderTest {
	@Autowired
	private BacktestTrader trader;
	@Autowired
	private BacktestAccount account;
	@Autowired
	private TickCache tickCache;
	private Security security;
	private static final double TICK_PRICE = 15;

	@Before
	public void setup() {
		SecurityBuilder securityBuilder = new SecurityBuilder();
		securityBuilder.setSymbol("ES");
		securityBuilder.setExchange("GLOBEX");
		securityBuilder.setExpiry(2015, Calendar.JUNE);
		securityBuilder.setSecurityType(SecurityType.FUTURE);
		security = securityBuilder.build();
		SecurityRegistry.add(security);

		TickBuilder tickBuilder = new TickBuilder();
		tickBuilder.setAskPrice(TICK_PRICE);
		tickBuilder.setBidPrice(TICK_PRICE);
		tickBuilder.setChange(0);
		tickBuilder.setDateTime(new Date());
		tickBuilder.setLastPrice(TICK_PRICE);
		tickBuilder.setOpenInterest(0);
		tickBuilder.setSecurity(security);
		tickBuilder.setSettlement(0);
		tickBuilder.setVolume(1);
		tickBuilder.setVolumeAsk(1);
		tickBuilder.setVolumeBid(1);
		tickCache.save(tickBuilder.build());

		PositionBuilder builder = new PositionBuilder();
		builder.setAccount("12345");
		builder.setAverageCost(TICK_PRICE);
		builder.setMarketPrice(TICK_PRICE);
		builder.setMarketValue(TICK_PRICE);
		builder.setQuantity(1);
		builder.setRealizedPnl(0);
		builder.setSecurity(security);
		builder.setUnrealizedPnl(0);
		account.getPositions().put(security.getKey(), builder.build());
	}

	@Test
	public void submitOrderTest_MarketOrder() {
		account.setupStartingFunds(TICK_PRICE + trader.getCommission());
		MarketOrder order = new MarketOrder(OrderAction.BUY, 1, security.getKey());
		trader.submitOrder(order);
		assertEquals(1, trader.getOrders(security.getKey()).size());
		assertEquals(OrderStatus.FILLED, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getOrderStatus());
		assertEquals(OrderStatusReason.OK_TO_FILL, trader.getOrders(security.getKey()).get(0).getLatestOrderState()
				.getOrderStatusReason());
		assertEquals(TICK_PRICE, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getLastFillPrice(), 0.01);
		assertEquals(1, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getQuantityFilled());
		assertEquals(0, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getQuantityRemaining());
		assertEquals(0, account.getAvailableFunds(), 0.01);
	}

	@Test
	public void submitOrderTest_MarketOrderInsufficientFunds() {
		account.setupStartingFunds(TICK_PRICE + trader.getCommission() - 1);
		MarketOrder order = new MarketOrder(OrderAction.BUY, 1, security.getKey());
		trader.submitOrder(order);
		assertEquals(1, trader.getOrders(security.getKey()).size());
		assertEquals(OrderStatus.CANCELLED, trader.getOrders(security.getKey()).get(0).getLatestOrderState()
				.getOrderStatus());
		assertEquals(OrderStatusReason.INSUFFICIENT_FUNDS, trader.getOrders(security.getKey()).get(0).getLatestOrderState()
				.getOrderStatusReason());
		assertEquals(0, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getQuantityFilled());
		assertEquals(1, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getQuantityRemaining());
	}

	@Test
	public void submitOrderTest_SellStopOrder() {
		double stopPrice = TICK_PRICE - 5;
		StopOrder order = new StopOrder(OrderAction.SELL, 1, security.getKey(), stopPrice);
		trader.submitOrder(order);

		// make sure the stop order is submitted but not filled
		assertEquals(1, trader.getOrders(security.getKey()).size());
		assertEquals(OrderStatus.SUBMITTED, trader.getOrders(security.getKey()).get(0).getLatestOrderState()
				.getOrderStatus());
		assertEquals(OrderStatusReason.ORDER_STATUS_PENDING, trader.getOrders(security.getKey()).get(0)
				.getLatestOrderState().getOrderStatusReason());
		assertEquals(0, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getQuantityFilled());
		assertEquals(1, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getQuantityRemaining());

		double nextTickPrice = stopPrice + 1;
		TickSubscriber tickSubscriber = WhiteboxImpl.getInternalState(trader, "catchAllTickSubscriber");
		List<Tick> ticks = buildTicksWithPrices(nextTickPrice);
		for (Tick tick : ticks) {
			tickCache.save(tick);
			tickSubscriber.tickReceived(tick);
		}

		// the order should still be unfilled
		assertEquals(OrderStatus.SUBMITTED, trader.getOrders(security.getKey()).get(0).getLatestOrderState()
				.getOrderStatus());
		assertEquals(OrderStatusReason.ORDER_STATUS_PENDING, trader.getOrders(security.getKey()).get(0)
				.getLatestOrderState().getOrderStatusReason());
		assertEquals(0, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getQuantityFilled());
		assertEquals(1, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getQuantityRemaining());

		nextTickPrice = stopPrice - 1;
		ticks = buildTicksWithPrices(nextTickPrice);
		for (Tick tick : ticks) {
			tickCache.save(tick);
			tickSubscriber.tickReceived(tick);
		}

		// the order should be filled now
		assertEquals(1, trader.getOrders(security.getKey()).size());
		assertEquals(OrderStatus.FILLED, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getOrderStatus());
		assertEquals(nextTickPrice, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getLastFillPrice(),
				0.01);
		assertEquals(OrderStatusReason.OK_TO_FILL, trader.getOrders(security.getKey()).get(0).getLatestOrderState()
				.getOrderStatusReason());
		assertEquals(1, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getQuantityFilled());
		assertEquals(0, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getQuantityRemaining());
	}

	@Test
	public void submitOrderTest_BuyStopOrder() {
		double stopPrice = TICK_PRICE + 5;
		account.setupStartingFunds(stopPrice + 1 + trader.getCommission());
		StopOrder order = new StopOrder(OrderAction.BUY, 1, security.getKey(), stopPrice);
		trader.submitOrder(order);

		// make sure the stop order is submitted but not filled
		assertEquals(1, trader.getOrders(security.getKey()).size());
		assertEquals(OrderStatus.SUBMITTED, trader.getOrders(security.getKey()).get(0).getLatestOrderState()
				.getOrderStatus());
		assertEquals(OrderStatusReason.ORDER_STATUS_PENDING, trader.getOrders(security.getKey()).get(0)
				.getLatestOrderState().getOrderStatusReason());
		assertEquals(0, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getQuantityFilled());
		assertEquals(1, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getQuantityRemaining());

		double nextTickPrice = stopPrice - 1;
		TickSubscriber tickSubscriber = WhiteboxImpl.getInternalState(trader, "catchAllTickSubscriber");
		List<Tick> ticks = buildTicksWithPrices(nextTickPrice);
		for (Tick tick : ticks) {
			tickCache.save(tick);
			tickSubscriber.tickReceived(tick);
		}

		// the order should still be unfilled
		assertEquals(OrderStatus.SUBMITTED, trader.getOrders(security.getKey()).get(0).getLatestOrderState()
				.getOrderStatus());
		assertEquals(OrderStatusReason.ORDER_STATUS_PENDING, trader.getOrders(security.getKey()).get(0)
				.getLatestOrderState().getOrderStatusReason());
		assertEquals(0, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getQuantityFilled());
		assertEquals(1, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getQuantityRemaining());

		nextTickPrice = stopPrice + 1;
		ticks = buildTicksWithPrices(nextTickPrice);
		for (Tick tick : ticks) {
			tickCache.save(tick);
			tickSubscriber.tickReceived(tick);
		}

		// the order should be filled now
		assertEquals(1, trader.getOrders(security.getKey()).size());
		assertEquals(OrderStatus.FILLED, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getOrderStatus());
		assertEquals(nextTickPrice, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getLastFillPrice(),
				0.01);
		assertEquals(OrderStatusReason.OK_TO_FILL, trader.getOrders(security.getKey()).get(0).getLatestOrderState()
				.getOrderStatusReason());
		assertEquals(1, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getQuantityFilled());
		assertEquals(0, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getQuantityRemaining());
	}

	@Test
	public void submitOrderTest_StopOrderInsufficientFunds() {
		double stopPrice = TICK_PRICE + 5;
		account.setupStartingFunds(0);
		StopOrder order = new StopOrder(OrderAction.BUY, 1, security.getKey(), stopPrice);
		trader.submitOrder(order);

		// make sure the stop order is submitted but not filled
		assertEquals(1, trader.getOrders(security.getKey()).size());
		assertEquals(OrderStatus.SUBMITTED, trader.getOrders(security.getKey()).get(0).getLatestOrderState()
				.getOrderStatus());
		assertEquals(OrderStatusReason.ORDER_STATUS_PENDING, trader.getOrders(security.getKey()).get(0)
				.getLatestOrderState().getOrderStatusReason());
		assertEquals(0, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getQuantityFilled());
		assertEquals(1, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getQuantityRemaining());

		double nextTickPrice = stopPrice + 1;
		TickSubscriber tickSubscriber = WhiteboxImpl.getInternalState(trader, "catchAllTickSubscriber");
		List<Tick> ticks = buildTicksWithPrices(nextTickPrice);
		for (Tick tick : ticks) {
			tickCache.save(tick);
			tickSubscriber.tickReceived(tick);
		}

		// the order should still be unfilled
		assertEquals(OrderStatus.CANCELLED, trader.getOrders(security.getKey()).get(0).getLatestOrderState()
				.getOrderStatus());
		assertEquals(OrderStatusReason.INSUFFICIENT_FUNDS, trader.getOrders(security.getKey()).get(0).getLatestOrderState()
				.getOrderStatusReason());
		assertEquals(0, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getQuantityFilled());
		assertEquals(1, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getQuantityRemaining());
	}

	@Test
	public void submitOrderTest_StopOrderImmediateFill() {
		double stopPrice = TICK_PRICE;
		account.setupStartingFunds(TICK_PRICE + trader.getCommission());
		StopOrder order = new StopOrder(OrderAction.BUY, 1, security.getKey(), stopPrice);
		trader.submitOrder(order);

		// make sure the stop order is submitted but not filled
		assertEquals(1, trader.getOrders(security.getKey()).size());
		assertEquals(OrderStatus.FILLED, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getOrderStatus());
		assertEquals(stopPrice, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getLastFillPrice(), 0.01);
		assertEquals(OrderStatusReason.OK_TO_FILL, trader.getOrders(security.getKey()).get(0).getLatestOrderState()
				.getOrderStatusReason());
		assertEquals(1, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getQuantityFilled());
		assertEquals(0, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getQuantityRemaining());
	}

	@Test
	public void submitOrderTest_BuyLimitOrder() {
		double limitPrice = TICK_PRICE - 5;
		LimitOrder order = new LimitOrder(OrderAction.BUY, 1, security.getKey(), limitPrice);
		trader.submitOrder(order);

		// make sure the limit order is submitted but not filled
		assertEquals(1, trader.getOrders(security.getKey()).size());
		assertEquals(OrderStatus.SUBMITTED, trader.getOrders(security.getKey()).get(0).getLatestOrderState()
				.getOrderStatus());
		assertEquals(OrderStatusReason.ORDER_STATUS_PENDING, trader.getOrders(security.getKey()).get(0)
				.getLatestOrderState().getOrderStatusReason());
		assertEquals(0, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getQuantityFilled());
		assertEquals(1, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getQuantityRemaining());

		double nextTickPrice = limitPrice + 1;
		TickSubscriber tickSubscriber = WhiteboxImpl.getInternalState(trader, "catchAllTickSubscriber");
		List<Tick> ticks = buildTicksWithPrices(nextTickPrice);
		for (Tick tick : ticks) {
			tickCache.save(tick);
			tickSubscriber.tickReceived(tick);
		}

		// the order should still be unfilled
		assertEquals(OrderStatus.SUBMITTED, trader.getOrders(security.getKey()).get(0).getLatestOrderState()
				.getOrderStatus());
		assertEquals(OrderStatusReason.ORDER_STATUS_PENDING, trader.getOrders(security.getKey()).get(0)
				.getLatestOrderState().getOrderStatusReason());
		assertEquals(0, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getQuantityFilled());
		assertEquals(1, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getQuantityRemaining());

		nextTickPrice = limitPrice - 1;
		ticks = buildTicksWithPrices(nextTickPrice);
		for (Tick tick : ticks) {
			tickCache.save(tick);
			tickSubscriber.tickReceived(tick);
		}

		// the order should be filled now
		assertEquals(1, trader.getOrders(security.getKey()).size());
		assertEquals(OrderStatus.FILLED, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getOrderStatus());
		assertEquals(nextTickPrice, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getLastFillPrice(),
				0.01);
		assertEquals(OrderStatusReason.OK_TO_FILL, trader.getOrders(security.getKey()).get(0).getLatestOrderState()
				.getOrderStatusReason());
		assertEquals(1, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getQuantityFilled());
		assertEquals(0, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getQuantityRemaining());
	}

	@Test
	public void submitOrderTest_SellLimitOrder() {
		double limitPrice = TICK_PRICE + 5;
		account.setupStartingFunds(limitPrice + 1 + trader.getCommission());
		LimitOrder order = new LimitOrder(OrderAction.SELL, 1, security.getKey(), limitPrice);
		trader.submitOrder(order);

		// make sure the limit order is submitted but not filled
		assertEquals(1, trader.getOrders(security.getKey()).size());
		assertEquals(OrderStatus.SUBMITTED, trader.getOrders(security.getKey()).get(0).getLatestOrderState()
				.getOrderStatus());
		assertEquals(OrderStatusReason.ORDER_STATUS_PENDING, trader.getOrders(security.getKey()).get(0)
				.getLatestOrderState().getOrderStatusReason());
		assertEquals(0, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getQuantityFilled());
		assertEquals(1, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getQuantityRemaining());

		double nextTickPrice = limitPrice - 1;
		TickSubscriber tickSubscriber = WhiteboxImpl.getInternalState(trader, "catchAllTickSubscriber");
		List<Tick> ticks = buildTicksWithPrices(nextTickPrice);
		for (Tick tick : ticks) {
			tickCache.save(tick);
			tickSubscriber.tickReceived(tick);
		}

		// the order should still be unfilled
		assertEquals(OrderStatus.SUBMITTED, trader.getOrders(security.getKey()).get(0).getLatestOrderState()
				.getOrderStatus());
		assertEquals(OrderStatusReason.ORDER_STATUS_PENDING, trader.getOrders(security.getKey()).get(0)
				.getLatestOrderState().getOrderStatusReason());
		assertEquals(0, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getQuantityFilled());
		assertEquals(1, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getQuantityRemaining());

		nextTickPrice = limitPrice + 1;
		ticks = buildTicksWithPrices(nextTickPrice);
		for (Tick tick : ticks) {
			tickCache.save(tick);
			tickSubscriber.tickReceived(tick);
		}

		// the order should be filled now
		assertEquals(1, trader.getOrders(security.getKey()).size());
		assertEquals(OrderStatus.FILLED, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getOrderStatus());
		assertEquals(nextTickPrice, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getLastFillPrice(),
				0.01);
		assertEquals(OrderStatusReason.OK_TO_FILL, trader.getOrders(security.getKey()).get(0).getLatestOrderState()
				.getOrderStatusReason());
		assertEquals(1, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getQuantityFilled());
		assertEquals(0, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getQuantityRemaining());
	}

	@Test
	public void submitOrderTest_LimitOrderInsufficientFunds() {
		double limitPrice = TICK_PRICE - 5;
		account.setupStartingFunds(0);
		LimitOrder order = new LimitOrder(OrderAction.BUY, 1, security.getKey(), limitPrice);
		trader.submitOrder(order);

		// make sure the limit order is submitted but not filled
		assertEquals(1, trader.getOrders(security.getKey()).size());
		assertEquals(OrderStatus.SUBMITTED, trader.getOrders(security.getKey()).get(0).getLatestOrderState()
				.getOrderStatus());
		assertEquals(OrderStatusReason.ORDER_STATUS_PENDING, trader.getOrders(security.getKey()).get(0)
				.getLatestOrderState().getOrderStatusReason());
		assertEquals(0, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getQuantityFilled());
		assertEquals(1, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getQuantityRemaining());

		double nextTickPrice = limitPrice - 1;
		TickSubscriber tickSubscriber = WhiteboxImpl.getInternalState(trader, "catchAllTickSubscriber");
		List<Tick> ticks = buildTicksWithPrices(nextTickPrice);
		for (Tick tick : ticks) {
			tickCache.save(tick);
			tickSubscriber.tickReceived(tick);
		}

		// the order should still be unfilled
		assertEquals(OrderStatus.CANCELLED, trader.getOrders(security.getKey()).get(0).getLatestOrderState()
				.getOrderStatus());
		assertEquals(OrderStatusReason.INSUFFICIENT_FUNDS, trader.getOrders(security.getKey()).get(0).getLatestOrderState()
				.getOrderStatusReason());
		assertEquals(0, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getQuantityFilled());
		assertEquals(1, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getQuantityRemaining());
	}

	@Test
	public void submitOrderTest_LimitOrderImmediateFill() {
		double limitPrice = TICK_PRICE;
		account.setupStartingFunds(TICK_PRICE + trader.getCommission());
		LimitOrder order = new LimitOrder(OrderAction.BUY, 1, security.getKey(), limitPrice);
		trader.submitOrder(order);

		// make sure the limit order is submitted but not filled
		assertEquals(1, trader.getOrders(security.getKey()).size());
		assertEquals(OrderStatus.FILLED, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getOrderStatus());
		assertEquals(limitPrice, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getLastFillPrice(), 0.01);
		assertEquals(OrderStatusReason.OK_TO_FILL, trader.getOrders(security.getKey()).get(0).getLatestOrderState()
				.getOrderStatusReason());
		assertEquals(1, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getQuantityFilled());
		assertEquals(0, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getQuantityRemaining());
	}

	@Test
	public void cancelOrderTest() {
		double limitPrice = TICK_PRICE - 5;
		account.setupStartingFunds(0);
		LimitOrder order = new LimitOrder(OrderAction.BUY, 1, security.getKey(), limitPrice);
		trader.submitOrder(order);

		// make sure the limit order is submitted but not filled
		assertEquals(1, trader.getOrders(security.getKey()).size());
		assertEquals(OrderStatus.SUBMITTED, trader.getOrders(security.getKey()).get(0).getLatestOrderState()
				.getOrderStatus());
		assertEquals(OrderStatusReason.ORDER_STATUS_PENDING, trader.getOrders(security.getKey()).get(0)
				.getLatestOrderState().getOrderStatusReason());
		assertEquals(0, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getQuantityFilled());
		assertEquals(1, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getQuantityRemaining());

		Order orderToCancel = trader.getOrders(security.getKey()).get(0);
		trader.cancelOrder(orderToCancel.getKey());
		assertEquals(OrderStatus.CANCELLED, trader.getOrders(security.getKey()).get(0).getLatestOrderState()
				.getOrderStatus());
		assertEquals(OrderStatusReason.MANUALLY_CANCELLED, trader.getOrders(security.getKey()).get(0).getLatestOrderState()
				.getOrderStatusReason());
		assertEquals(0, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getQuantityFilled());
		assertEquals(1, trader.getOrders(security.getKey()).get(0).getLatestOrderState().getQuantityRemaining());
	}

	private List<Tick> buildTicksWithPrices(double... prices) {
		List<Tick> ticks = new ArrayList<Tick>();
		for (double price : prices) {
			TickBuilder tickBuilder = new TickBuilder();
			tickBuilder.setAskPrice(price);
			tickBuilder.setBidPrice(price);
			tickBuilder.setChange(0);
			tickBuilder.setDateTime(new Date());
			tickBuilder.setLastPrice(price);
			tickBuilder.setOpenInterest(0);
			tickBuilder.setSecurity(security);
			tickBuilder.setSettlement(0);
			tickBuilder.setVolume(1);
			tickBuilder.setVolumeAsk(1);
			tickBuilder.setVolumeBid(1);
			ticks.add(tickBuilder.build());
		}
		return ticks;
	}
}
