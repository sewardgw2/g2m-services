package com.g2m.services.tradingservices.utilities;

import java.util.ArrayList;
import java.util.List;

import com.g2m.services.tradingservices.SecurityRegistry;
import com.g2m.services.tradingservices.entities.Security;
import com.g2m.services.tradingservices.entities.Security.SecurityBuilder;
import com.g2m.services.tradingservices.enums.SecurityType;

/**
 * Added 4/19/2015.
 * 
 * @author Michael Borromeo
 */
public class SecurityUtility {
	public static List<Security> createAndRegisterSecurities(String... symbols) {
		if (0 == symbols.length) {
			return new ArrayList<Security>();
		}

		List<Security> securities = new ArrayList<Security>();
		SecurityBuilder securityBuilder = new SecurityBuilder();
		securityBuilder.setSymbol(symbols[0]);
		securityBuilder.setExchange("GLOBEX");
		securityBuilder.setSecurityType(SecurityType.STOCK);
		Security security = securityBuilder.build();
		SecurityRegistry.add(security);
		securities.add(security);

		for (int i = 1; i < symbols.length; i++) {
			securityBuilder.setSymbol(symbols[i]);
			security = securityBuilder.build();
			SecurityRegistry.add(security);
			securities.add(security);
		}

		return securities;
	}
}
