package com.g2m.services.tradingservices.enums;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Michael Borromeo
 */
public enum BarSize {
	_1_SEC(1), _5_SECS(5), _10_SECS(10), _15_SECS(15), _30_SECS(30), _1_MIN(60), _2_MINS(120), _3_MINS(180), _5_MINS(300),
	_10_MINS(600), _15_MINS(900), _20_MINS(1200), _30_MINS(1800), _1_HOUR(3600), _4_HOURS(14400), _1_DAY(86400);

	private int secondsInBar;
	static List<BarSize> registeredBarSizes;

	BarSize(int secondsInBar) {
		this.secondsInBar = secondsInBar;
	}

	@Override
	public String toString() {
		return super.toString();
	}

	public String toFormattedString() {
		return super.toString().substring(1).replace('_', ' ');
	}

	public int getSecondsInBar() {
		return this.secondsInBar;
	}

	/*
	 * We'll only register the specific bar sizes that are needed by 
	 * a strategy so that we don't have too much unnecessary data
	 * floating around the system.
	 */
	public static void registerSpecificBarSize(List<BarSize> barSizes){
		registeredBarSizes = new ArrayList<BarSize>();
		if(barSizes != null)
			for (BarSize bs : barSizes)
				registeredBarSizes.add(bs);
	}

	public static List<BarSize> registerAllSecurities(boolean usingTicks){

		registeredBarSizes = new ArrayList<BarSize>();

		if(usingTicks){
			registeredBarSizes.add(BarSize._1_SEC);
			registeredBarSizes.add(BarSize._5_SECS);
			registeredBarSizes.add(BarSize._10_SECS);
			registeredBarSizes.add(BarSize._15_SECS);
			registeredBarSizes.add(BarSize._30_SECS);
		}
		registeredBarSizes.add(BarSize._1_MIN);
		registeredBarSizes.add(BarSize._2_MINS);
		registeredBarSizes.add(BarSize._3_MINS);
		registeredBarSizes.add(BarSize._5_MINS);
		registeredBarSizes.add(BarSize._10_MINS);
		registeredBarSizes.add(BarSize._15_MINS);
		registeredBarSizes.add(BarSize._20_MINS);
		registeredBarSizes.add(BarSize._30_MINS);
		registeredBarSizes.add(BarSize._1_HOUR);
		registeredBarSizes.add(BarSize._4_HOURS);
		registeredBarSizes.add(BarSize._1_DAY);

		return registeredBarSizes;
	}

	public static List<BarSize> getNextBarSizes(BarSize barSize){

		List<BarSize> returnBarSize = new ArrayList<BarSize>();
		switch(barSize){
		case _1_SEC: 
			if(registeredBarSizes.contains(_5_SECS))
				returnBarSize.add(_5_SECS);
			break;
		case _5_SECS: 
			if(registeredBarSizes.contains(_10_SECS))
				returnBarSize.add(_10_SECS);
			if(registeredBarSizes.contains(_15_SECS))
				returnBarSize.add(_15_SECS);		
			break;
		case _10_SECS:
			break;
		case _15_SECS: 
			if(registeredBarSizes.contains(_30_SECS))
				returnBarSize.add(_30_SECS);
			break;
		case _30_SECS: 
			if(registeredBarSizes.contains(_1_MIN))
				returnBarSize.add(_1_MIN);
			break;		
		case _1_MIN: 
			if(registeredBarSizes.contains(_2_MINS))
				returnBarSize.add(_2_MINS);
			if(registeredBarSizes.contains(_3_MINS))
				returnBarSize.add(_3_MINS);
			if(registeredBarSizes.contains(_5_MINS))
				returnBarSize.add(_5_MINS);
			break;		
		case _5_MINS:
			if(registeredBarSizes.contains(_10_MINS))
				returnBarSize.add(_10_MINS);
			if(registeredBarSizes.contains(_15_MINS))
				returnBarSize.add(_15_MINS);
			break;
		case _10_MINS:
			if(registeredBarSizes.contains(_20_MINS))
				returnBarSize.add(_20_MINS);
			break;
		case _15_MINS:
			if(registeredBarSizes.contains(_30_MINS))
				returnBarSize.add(_30_MINS);
			break;
		case _30_MINS:
			if(registeredBarSizes.contains(_1_HOUR))
				returnBarSize.add(_1_HOUR);
			break;
		case _1_HOUR:
			if(registeredBarSizes.contains(_4_HOURS))
				returnBarSize.add(_4_HOURS);
			break;
		case _4_HOURS:
			if(registeredBarSizes.contains(_1_DAY))
				returnBarSize.add(_1_DAY);
			break;
		default:
			break;
		}
		return returnBarSize;
	}
}
